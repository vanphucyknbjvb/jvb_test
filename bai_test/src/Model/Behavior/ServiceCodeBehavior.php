<?php

/**
 * Short description for ServiceCodeBehavior
 *
 * PHP version 7.2.2
 *
 * @category  phms_ct-portal
 * @package   CakePHP 3
 * @author    Linhntk <n.khanhlinh@jvb-corp.com>
 * @copyright 2018 JVB
 * @license   http://jvb-corp.com JVB License 1.0
 * @link      http://jvb-corp.com
 * @date      Apr 04, 2018
 */

namespace App\Model\Behavior;

use Cake\Datasource\EntityInterface;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use ArrayObject;
use Cake\Collection\Collection;
use Cake\Datasource\QueryInterface;
use Cake\I18n\I18n;
use Cake\ORM\Entity;
use Cake\ORM\PropertyMarshalInterface;
use Cake\ORM\Query;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Table;
use Cake\Utility\Inflector;

/**
 * CakePHP ServiceBehavior
 * @author Linhntk <n.khanhlinh@jvb-corp.com>
 */
class ServiceCodeBehavior extends Behavior
{
    
    /**
     * Default config
     *
     * These are merged with user-provided configuration when the behavior is used.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'field' => 'service_id'
    ];
    protected $_serviceId = null;
    
    /**
     * Constructor
     *
     * @param \Cake\ORM\Table $table The table this behavior is attached to.
     * @param array $config The config for this behavior.
     */
    public function __construct(Table $table, array $config = [])
    {   
        if (!empty($_SESSION['Auth']['Admin']['service_id'])) {
            $this->_serviceId = $_SESSION['Auth']['Admin']['service_id'];
        }
        if (empty($this->_serviceId)
                && defined('DEFAULT_SERVICE_ID')) {
            $_SESSION['Auth']['Admin']['service_id'] = DEFAULT_SERVICE_ID;
            $this->_serviceId = DEFAULT_SERVICE_ID;
        }
        parent::__construct($table, $config);
    }
    
    /**
     * Run before a model is about to be find, used only fetch for non-deleted records.
     *
     * @param \Cake\Event\Event $event The beforeFind event that was fired.
     * @param \Cake\ORM\Query $query Query
     * @param \ArrayObject $options The options for the query
     * @return void
     */
    public function beforeFind(Event $event, Query $query, $options)
    {
        $alias = $this->_table->getAlias();
        $cond = [$alias.'.'.$this->_config['field'] => $this->_serviceId];
        $include = false;
        if($this->_serviceId && $this->_table->hasField($this->_config['field'])){
            if(!empty($query->clause('where'))){
                $queryEx = $query->clause('where');
                $binder = $query->getValueBinder();
                $where = $queryEx->sql($binder);
                if (!empty($where) && is_string($where)) {
                    $include = true;
                    $fields = array(
                        $alias.'.'.$this->_config['field'],
                        $this->_config['field']
                    );
                    foreach ($fields as $field) {
                        if (preg_match('/^' . preg_quote($field) . '[\s=!]+/i', $where) || preg_match('/\\x20+' . preg_quote($field) . '[\s=!]+/i', $where) || strpos($where, $field)) {
                            $include = false;
                            break;
                        }
                    }
                } else if (empty($where) || (!in_array($this->_config['field'], array_keys($where)) && !in_array($alias.'.'.$this->_config['field'], array_keys($where)))) {
                    $include = true;
                }
            } else{
                $include = true;
            }
        }
        if ($include) {
            if (empty($query->clause('where'))) {
                $query->where(array());
            }
            // Added by COMPS for using service_group
            if (!array_key_exists('ignore_service_id', $_SESSION)) {
                $query->where($cond);
            }
        }
        return $query;
    }

    /**
     * Run before a model is saved, used to disable beforeFind() override.
     *
     * @param \Cake\Event\Event $event The beforeSave event that was fired
     * @param \Cake\Datasource\EntityInterface $entity The entity that is going to be saved
     * @param \ArrayObject $options the options passed to the save method
     * @return void
     */
    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
         /*主キーが存在する場合はtrue*/
        $alias = $this->_table->getAlias();
        $cond = [$alias.'.'.$this->_config['field'] => $this->_serviceId];
        if(!empty($entity['id'])) {
            return true;
        }
        if( !empty($entity[$this->_config['field']]) ){
          return true;
        }
        if ( !isset($this->_serviceId)  || !$this->_table->hasField($this->_config['field'])) {
            echo "SYSTEM ERROR.SERVICE CODE IS NULL";
            return false;
        }
        
        $entity[$this->_config['field']] = $this->_serviceId;
        return true;
    }

}
