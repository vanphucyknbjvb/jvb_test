
あなたが申請した審査の内容が、公開されました。

課題名：<?php echo $shinsei['review']['short_name'] ?>

<?php 
    $irbUrlNew = $irbUrls['shinseis_detail_review'];
    $irbUrlNew['?'] = array('id'=>$shinsei['review']['shinsei_id']);
    echo $this->Url->build($irbUrlNew, true) ?>

------------------------------------------------------------
【お問合せ先】
臨床研究倫理委員会事務局（病院研究国際室）
内線：2513
E-mail：gaibushikin@ml.nagasaki-u.ac.jp
