先日、通知しております下記課題の継続または終了報告について、申請期限を過ぎています。
早急に申請して下さい。

【整理番号】((整理番号))

【課題名】((課題名))

継続申請作成用URL
((継続申請URL))

終了申請作成用URL
((終了申請URL))


------------------------------------------------------------
筑波大学附属病院 
臨床研究推進・支援センター（内線7562）
E-mail：rinshokenkyu@un.tsukuba.ac.jp