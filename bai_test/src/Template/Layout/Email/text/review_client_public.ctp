
あなたの担当する審査の内容が、公開されました。
 
課題名：<?php echo $review['name'] ?>

<?php 
    $irbUrlNew = $irbUrls['reviews_detail'];
    $irbUrlNew['?'] = array('id'=>$review['id']);
    echo $this->Url->build($irbUrlNew, true) ?>

<?php if ( $footer): ?><?php echo $footer ?><?php else: ?>
------------------------------------------------------------
治験・臨床研究支援クラウドサービス　CT-Portal.com
Clinical Trial Portal Service
<?php endif; ?>

<?php $this->Url->build($irbUrls['home'], true) ?>

