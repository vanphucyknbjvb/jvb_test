<?php echo $user['name'] ?> 様

治験・臨床研究支援クラウドサービスCT-Portal をご利用ありがとうございます。

登録されているメールアドレスとパスワードは下記のとおりです。

メールアドレス：<?php echo $user['mail']."\n" ?>
パスワード：<?php echo $user['password']."\n"?>

下記からログインしてください。

<?php echo $url_login ?>

------------------------------------------------------------
治験・臨床研究支援クラウドサービス　CT-Portal.com
Clinical Trial Portal Service

<?php echo $url_home ?>

