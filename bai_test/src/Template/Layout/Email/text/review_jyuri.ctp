<?php if ($shinsei['board_group_id'] == 302) :?>
<?php echo $shinsei['doctor'] ?>先生

量研機構臨床研究審査委員会事務局よりご連絡です。

下記の申請を受理しました。

整理番号：<?php echo $shinsei['exam_num'] ?>

jRCT番号：<?php echo ($shinsei['Exam']['jrct_num']) ? $shinsei['Exam']['jrct_num'] : '' ?>

課題名：<?php echo $shinsei['name'] ?>


参照メンバーの追加は以下よりログイン
<?php echo $this->Url->build($irbUrls['exams_detail_member']+array('?'=>array('id'=>$shinsei['exam_id'])), true) ?>

申請情報は以下よりログイン
<?php echo $this->Url->build($irbUrls['shinseis_detail']+array('?'=>array('id'=>$shinsei['id'])), true) ?>


お問い合わせは、以下の事務局メーリングリストまでお送りください。

<?php else :?>
<?php echo $shinsei['doctor'] ?>様

このメールをお送りしているアドレスは送信専用です。
お問い合わせは以下にお送りください。
helsinki@qst.go.jp

下記の申請を受理いたしました。

課題名：<?php echo $shinsei['name'] ?>

参照メンバーの追加は以下よりログイン
<?php echo $this->Url->build($irbUrls['exams_detail_member']+array('?'=>array('id'=>$shinsei['exam_id'])), true) ?>

申請情報は以下よりログイン
<?php echo $this->Url->build($irbUrls['shinseis_detail']+array('?'=>array('id'=>$shinsei['id'])), true) ?>


<?php endif;?>
---------------------------
臨床研究審査委員会事務局
helsinki@qst.go.jp
---------------------------



