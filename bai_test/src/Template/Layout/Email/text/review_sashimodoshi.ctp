<?php if ($shinsei['board_group_id'] == 302) :?>
<?php echo $shinsei['doctor'] ?>先生

量研機構臨床研究審査委員会事務局よりご連絡です。

下記の課題に関して、申請を差し戻しました。
必要な場合は、修正を行い、再度申請をしていただけますようお願いします。

整理番号：<?php echo $shinsei['exam_num'] ?>

jRCT番号：<?php echo ($shinsei['Exam']['jrct_num']) ? $shinsei['Exam']['jrct_num'] : '' ?>

課題名：<?php echo $shinsei['name'] ?>


修正申請は以下よりログイン
<?php echo $this->Url->build($irbUrls['shinseis_detail_check']+array('?'=>array('id'=>$shinsei['id'])), true) ?>

お問い合わせは、以下の事務局メーリングリストまでお送りください。 

<?php else :?>
<?php echo $shinsei['doctor'] ?>様

このメールをお送りしているアドレスは送信専用です。
お問い合わせは以下にお送りください。
helsinki@qst.go.jp

下記の課題に関して、申請を差し戻しました。
必要な場合は、修正を行い、再度申請をしていただけますようお願いいたします。

課題名：<?php echo $shinsei['name'] ?>

修正申請は以下よりログイン
<?php echo $this->Url->build($irbUrls['shinseis_detail_check']+array('?'=>array('id'=>$shinsei['id'])), true) ?>

ご不明な点がある場合は事務局までご連絡いただけますようお願いいたします。

<?php endif;?>
---------------------------
臨床研究審査委員会事務局
helsinki@qst.go.jp
---------------------------



