
申請内容が確認できました。
捺印が必要な申請書類（各種様式）をご準備のうえ、事務局までご提出ください。
捺印済み資料の提出をもって、手続きが完了いたします。

課題名：<?php echo $shinsei['name'] ?>

<?php 
    $irbUrlNew = $irbUrls['shinseis_detail'];
    $irbUrlNew['?'] = array('id'=>$shinsei['id']);
    echo $this->Url->build($irbUrlNew, true) ?>

<?php if ( $footer): ?><?php echo $footer ?><?php else: ?>
------------------------------------------------------------
治験・臨床研究支援クラウドサービス　CT-Portal.com
Clinical Trial Portal Service
<?php endif; ?>

<?php $this->Url->build($irbUrls['home'], true) ?>

