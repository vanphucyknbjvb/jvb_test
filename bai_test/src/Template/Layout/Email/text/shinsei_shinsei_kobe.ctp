
申請依頼が届きました。

<?php
  $type = "";
  switch ($shinsei['shinsei_type']) {
    case 1:
      $type = '新規';
      break;
    case 2:
      $type = '変更';
      break;
    case 3:
      $type = '逸脱';
      break;
    case 4:
      $type = '重篤';
      break;
    case 5:
      $type = '継続';
      break;
    case 91:
      $type = '修正';
      break;
    case 99:
      $type = '終了';
      break;
  }
?>
課題名：<?php echo $shinsei['name'] ?>【<?php echo $type ?>】

<?php 
    $irbUrlNew = $irbUrls['shinseis_detail'];
    $irbUrlNew['?'] = array('id'=>$shinsei['id']);
    echo $this->Url->build($irbUrlNew, true) ?>

------------------------------------------------------------
治験・臨床研究支援クラウドサービス　CT-Portal.com
Clinical Trial Portal Service

<?php echo $this->Url->build($irbUrls['home'], true) ?>

