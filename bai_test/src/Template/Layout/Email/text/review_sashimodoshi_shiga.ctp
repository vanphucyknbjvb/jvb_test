
以下のＵＲＬにアクセスして、コメントにご対応の上、再申請してください。

課題名：<?php echo $shinsei['name'] ?>

<?php 
    $irbUrlNew = $irbUrls['shinseis_detail'];
    $irbUrlNew['?'] = array('id'=>$shinsei['id']);
    echo $this->Url->build($irbUrlNew, true) ?>
※CT-Portalに未ログインの場合は、以下よりログイン後、上記URLをクリックしてください。
https://nx.ct-portal.com/users/shiga_rinsho_login 
サービスコードの入力が必要な画面が表示された場合「shiga-rinsho」を入力してください。

<?php if ( $footer): ?><?php echo $footer ?><?php else: ?>
------------------------------------------------------------
治験・臨床研究支援クラウドサービス　CT-Portal.com
Clinical Trial Portal Service
<?php endif; ?>

<?php $this->Url->build($irbUrls['home'], true) ?>

