
治験・臨床研究支援クラウドサービスCT-Portal の管理者が
あなたをユーザ登録しました。


メールアドレス：<?php echo $user['mail'] ?>
ログインID：    <?php echo $user['user_id'] ?>
パスワード：    <?php echo $user['password'] ?>
サービスコード：<?php echo $service_code ?>


下記からログインしてご利用になれます。

<?php echo $this->Url->build($irbUrls['users_login'], true) ?>


------------------------------------------------------------
治験・臨床研究支援クラウドサービス　CT-Portal.com
Clinical Trial Portal Service

<?php echo $this->Url->build($irbUrls['home'], true) ?>


