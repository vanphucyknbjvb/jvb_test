
資料を受け付けました。

なお、提出資料に不足等があった場合は、事務局より連絡があります。


【整理番号】<?php echo $shinsei['exam_num'] ?>

【申請タイプ】<?php echo $shinseiType[$shinsei['shinsei_type']] ?>

【申請日】<?php echo $this->Date->datecb($shinsei['shinsei_date']) ?>

【申請者】<?php echo $shinsei['shinseisya_name'] ?>

【課題名】<?php echo $shinsei['name'] ?>

<?php echo $url_shinsei ?>


------------------------------------------------------------
筑波大学附属病院 
臨床研究推進・支援センター（内線7562）
E-mail：rinshokenkyu@un.tsukuba.ac.jp

