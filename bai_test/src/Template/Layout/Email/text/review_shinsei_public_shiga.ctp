
あなたが申請した審査の内容が、公開されました。

課題名：<?php echo $shinsei['review']['name'] ?>

<?php 
    $irbUrlNew = $irbUrls['shinseis_detail_review'];
    $irbUrlNew['?'] = array('id'=>$shinsei['review']['shinsei_id']);
    echo $this->Url->build($irbUrlNew, true) ?>
※CT-Portalに未ログインの場合は、以下よりログイン後、上記URLをクリックしてください。
https://nx.ct-portal.com/users/shiga_rinsho_login 
サービスコードの入力が必要な画面が表示された場合「shiga-rinsho」を入力してください。

尚、修正後承認・保留となった件については、
http://gakunai.shiga-med.ac.jp/~hqrec/download/5.reapplication.pdf
参照の上、再申請をお願いします。

<?php if ( $footer): ?><?php echo $footer ?><?php else: ?>
------------------------------------------------------------
治験・臨床研究支援クラウドサービス　CT-Portal.com
Clinical Trial Portal Service
<?php endif; ?>

<?php $this->Url->build($irbUrls['home'], true) ?>

