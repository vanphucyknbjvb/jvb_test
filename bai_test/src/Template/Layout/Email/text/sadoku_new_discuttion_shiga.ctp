
以下のＵＲＬにアクセスして、ブラッシュアップ担当者からの質問にご回答ください。

事前審査委員会名：<?php echo $sadokuhan_name['Sadokuhan']['name']?>

申請課題名：<?php echo $shinsei['name'] ?>


<?php echo $this->Url->build($irbUrls['sadoku_index'], true);?>?id=<?php echo $this->request->params['form']['shinseiId'];?>
※CT-Portalに未ログインの場合は、以下よりログイン後、上記URLをクリックしてください。
https://nx.ct-portal.com/users/shiga_rinsho_login 
サービスコードの入力が必要な画面が表示された場合「shiga-rinsho」を入力してください。


------------------------------------------------------------
治験・臨床研究支援クラウドサービス　CT-Portal.com
Clinical Trial Portal Service

<?php echo $this->Url->build($irbUrls['home'], true) ?>

