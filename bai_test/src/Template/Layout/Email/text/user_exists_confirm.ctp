<?php echo $user['department_name'] ?><?php echo $user['job'] ?> <?php echo $user['name'] ?> 様

IRBサポートシステムに登録されているユーザ情報が
有効なものなのかを確認するためにお送りしております。

このメールを受信された場合は、下記のリンクをクリックしてください。

<?php echo $url_exists_confirm ?>


リンクをクリックしていただかないと、
IRBサポートシステムにログインできなくなることがあります。

ご協力ありがとうございました。

------------------------------------------------------------
治験・臨床研究支援クラウドサービス　CT-Portal.com
Clinical Trial Portal Service

<?php echo $url_home ?>

