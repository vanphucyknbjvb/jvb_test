
申請が差し戻しされました。
下記URLからコメント欄を参照の上、「申請内容を編集」より修正等をお願いします。
修正後、再度提出してください。

注：本メールは、申請時に入力していただきました、
「申請者情報」及び「入力者情報」欄のメールアドレスに送信していますが、
「申請内容を編集」ができるのは、「入力者」のみです。
「申請者」は編集できませんのでご留意ください。

<?php echo $url_shinsei_check ?>


【整理番号】<?php echo $shinsei['exam_num'] ?>

【申請タイプ】<?php echo $shinseiType[$shinsei['shinsei_type']] ?>

【申請日】<?php echo $this->Date->datecb($shinsei['shinsei_date']) ?>

【申請者】<?php echo $shinsei['shinseisya_name'] ?>

【課題名】<?php echo $shinsei['name'] ?>

<?php echo $url_shinsei ?>


------------------------------------------------------------
筑波大学附属病院 
臨床研究推進・支援センター（内線7562）
E-mail：rinshokenkyu@un.tsukuba.ac.jp

