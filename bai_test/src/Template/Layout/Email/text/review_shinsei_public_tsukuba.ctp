
貴殿が申請した審査の結果が、公開されました。
下記URLから審査結果のご確認をお願いします。

<?php 
    $irbUrlNew = $irbUrls['shinseis_detail_review'];
    $irbUrlNew['?'] = array('id'=>$shinsei['id']);
    echo $this->Url->build($irbUrlNew, true) ?>


また、「一部修正後、承認」及び「再審査」となった場合は、
上記URLの審査結果欄に修正依頼を添付していますので、ご確認の上、
「再申請」より修正をお願いします。
 修正後は、再申請を行ってください。


【整理番号】<?php echo $shinsei['exam_num'] ?>

【申請タイプ】<?php echo $shinseiType[$shinsei['shinsei_type']] ?>

【申請日】<?php echo $this->Date->datecb($shinsei['shinsei_date']) ?>

【申請者】<?php echo $shinsei['shinseisya_name'] ?>

【課題名】<?php echo $shinsei['name'] ?>

<?php 
    $irbUrlNewD = $irbUrls['shinseis_detail'];
    $irbUrlNewD['?'] = array('id'=>$shinsei['id']);
    echo $this->Url->build($irbUrlNewD, true) ?>


------------------------------------------------------------
筑波大学附属病院 
臨床研究推進・支援センター（内線7562）
E-mail：rinshokenkyu@un.tsukuba.ac.jp

