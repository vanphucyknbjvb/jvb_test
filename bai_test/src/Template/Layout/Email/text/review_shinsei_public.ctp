<?php if ($shinsei['board_group_id'] == 302) :?>
<?php echo $shinsei['doctor'] ?>先生

量研機構臨床研究審査委員会事務局よりご連絡です。

審査結果を通知します。
整理番号：<?php echo $shinsei['exam_num'] ?>

jRCT番号：<?php echo ($shinsei['Exam']['jrct_num']) ? $shinsei['Exam']['jrct_num'] : '' ?>

課題名：<?php echo $shinsei['name'] ?>

研究責任者名：<?php echo $shinsei['doctor'] ?>

<?php echo $this->Url->build($irbUrls['shinseis_detail_review']+array('?'=>array('id'=>$shinsei['id'])), true) ?>

上記よりログインし、下記の■お願い■事項を行ってください。


■お願い■
○システムから審査結果通知書（PDF）を確認してください。

○審査結果が、「継続審査」の場合は、「再申請」ボタンより再申請を行なってください。

○新規申請の場合は、研究に関するjRCT登録と厚生局への届出が必要です。
jRCT登録と届出を行い、下記よりjRCT番号と届出日を記入してください。
jRCT番号、届出日記入は以下よりログイン
<?php echo $this->Url->build($irbUrls['exams_detail']+array('?'=>array('id'=>$shinsei['exam_id'])), true) ?>

（非特定臨床研究は、厚生局への届出は不要とされています。jRCT番号のみご記入ください。）
なお、jRCT登録内容の変更が必要な時には、変更申請を行い、承認された情報をjRCTに登録してください。

お問い合わせは、以下の事務局メーリングリストまでお送りください。 

<?php else :?>
<?php echo $shinsei['doctor'] ?>様

このメールをお送りしているアドレスは送信専用です。
お問い合わせは以下にお送りください。
helsinki@qst.go.jp

審査結果を通知します。
研究計画書番号：<?php echo $shinsei['exam_num'] ?>　
課題名：<?php echo $shinsei['name'] ?>　
研究責任者名：<?php echo $shinsei['doctor'] ?>

<?php echo $this->Url->build($irbUrls['shinseis_detail_review']+array('?'=>array('id'=>$shinsei['id'])), true) ?>

上記よりログインし、下記の■お願い■事項を行ってください。


■お願い■
○システムから審査結果通知書（PDF）を確認してください。

○審査結果が、「修正の上で承認」の場合は、研06 研究計画書等修正報告書と必要資料をご準備いただき、上記URLの「再申請」ボタンよりご提出ください。

○新規申請で、データベース登録・公開の予定有と申請した場合は、研究実施に先だって研究に関する臨床試験登録が必要です。
登録を行い、下記よりUMIN試験ID等を記入してください。
UMIN試験ID等記入は以下よりログイン
<?php echo $this->Url->build($irbUrls['exams_detail']+array('?'=>array('id'=>$shinsei['exam_id'])), true) ?>

なお、登録内容については、変更申請または研究の進捗に応じて適宜更新し、研究終了時も遅滞なく、研究の結果を登録してください。

ご不明な点がある場合は事務局までご連絡いただけますようお願いいたします。

<?php endif;?>
---------------------------
臨床研究審査委員会事務局
helsinki@qst.go.jp
---------------------------




