<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<title><?php echo $this->fetch('title') ?></title>
<?php 
    echo $this->Html->css(array(
                'style_conf',
                'style_contents',
                'style_headfootlogin','tablecloth', 'jquery-ui-1.12.1.custom', 'uniform.default', 'tooltip',
                'style_custom'
            ));
    echo $this->Html->script(array(
                'jquery-3.3.1','jquery-3.3.1.min',
                'jquery-ui-1.12.1.custom.min',
                //'bootstrap','bootstrap.min', 
                'autoresize.jquery.min', 'textarea', 
                'jquery.uniform.4.2.2', 'jquery.leanModal.min', 
                'IOS','common',
                'jquery.ui.datepicker-ja', 'jquery_calendar'));
?>
<script type="text/javascript">
$.ajaxSetup({ cache: false });
</script>
</head>
<body id="popuptab"> 

<div>

<!--- contents start --->
<?php echo $this->fetch('content');?>
<!--- contents end --->

</div>

</body>
</html>
