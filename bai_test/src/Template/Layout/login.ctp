<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja">
    <head>
        <meta http-equiv="content-language" content="ja" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="content-script-type" content="text/javascript" />
        <meta http-equiv="content-style-type" content="text/css" />
        <meta name="robots" content="noindex" />
        <title><?php echo $this->fetch('title') ?></title>
        <?php 
            echo $this->Html->script(array(
                'jquery-3.3.1','jquery-3.3.1.min',
                //'bootstrap','bootstrap.min'
            ));
            echo $this->Html->css(array(
                'style_conf',
                'style_contents',
                'style_headfootlogin',
                'style_custom'
            ));
        ?>
    </head>
    <body>
        <div id="ngsn_wrapper">
            <div id="ngsn_content">
                <a name="top" id="ngsn_top"></a>
                <div id="ngsn_header">
                    <div id="ngsn_header_logo">
                        <?php
                            if($this->request->here == '/users/nagasaki_nintei_login' || $this->Config->getConfigService("login.expand.login_url") == '/users/nagasaki_nintei_login' ) {
                                echo $this->Html->link(
                                    $this->Html->image('/img2/logo_nagasaki_tokutei.jpg',
                                        array('alt' => 'CT-Portal', 'style' => 'width: 427px; height: 64px;')),
                                        '',
                                        array('escape' => false)
                                );
                            } else {
                                echo $this->Html->link(
                                    $this->Html->image('/img2/site-logo.jpg',
                                        array('alt' => 'CT-Portal', 'style' => 'width: 427px; height: 64px;')),
                                        '',
                                        array('escape' => false)
                                );
                            }
                        ?>
                    </div>
                </div>
                <div id="ngsn_main">
                    <?php echo $this->fetch('content');?>
                </div>
            </div>
            <div id="ngsn_footer">
                <div id="ngsn_footer_area">
                    <?php
                        echo $this->Html->image(
                            '/img2/footer-logo.png',
                            array('alt' => 'powerd by PharmaMedicalSolution', 'style' => 'width: 344px; height: 38px;')
                        );
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>