<?php /**
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<title><?php echo $title_for_layout; ?></title>
<?php echo $scripts_for_layout ?>
<script type="text/javascript">
$.ajaxSetup({ cache: false });
</script>
</head>
<body id="popup">
*/ ?>

<div id="popup">

<!--- contents start --->
<?php echo $this->fetch('content');?>
<!--- contents end --->

</div>

<?php /**
</body>
</html>
*/ ?>
