<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<body>
    <?php
        echo $this->Form->create('form1',
            [
                'type' => 'POST',
                'id' => 'form1',
            ]
        );
    ?>    
    <div id="login">
        <h3 class="text-center text-white pt-5">Login form JVB</h3>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" action="" method="post">
                            <h3 class="text-center text-info">Login</h3>
                            <?php echo $this->Flash->render() ?>
                            <div class="form-group">
                                <label for="username" class="text-info">Username:</label><br>
                                <?php echo $this->Form->control('username',array(
                                    'type'=>'text',
                                    'label'=>false,
                                    'class'=>'form-control',
                                    'templates' => ['inputContainer' => '{{content}}'])); 
                                ?>                                    
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <?php echo $this->Form->control('password',array(
                                    'type'=>'password',
                                    'label'=>false,
                                    'class'=>'form-control',
                                    'id' =>'password',
                                    'templates' => ['inputContainer' => '{{content}}'])); 
                                ?>                                     
                            </div>
                            <div class="form-group">
                                <input type="submit" name="submit" class="btn btn-info btn-md" value="submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>    
</body>
<style type="text/css">
    body {
      margin: 0;
      padding: 0;
      background-color: #17a2b8;
      height: 100vh;
    }
    #login .container #login-row #login-column #login-box {
      margin-top: 120px;
      max-width: 600px;
      height: 320px;
      border: 1px solid #9C9C9C;
      background-color: #EAEAEA;
    }
    #login .container #login-row #login-column #login-box #login-form {
      padding: 20px;
    }
    #login .container #login-row #login-column #login-box #login-form #register-link {
      margin-top: -85px;
    }    
</style>