<?php echo $this->Html->css(['select2.min.css']) ?>
<?php echo $this->Html->script('select2.min.js') ?>
<h2>Thêm Tin tức</h2><br />
<?= $this->Flash->render() ?>
<?= $this->Form->create(isset($saveBlog) ? $saveBlog : '',['type'=>'file']) ?>
<?php $this->Form->setTemplates(['inputContainer' => '{{content}}']); ?>
    <div class="form-group">
        <label class="control-label">Tiêu đề<span class="required">*</span></label>
        <?php echo $this->Form->control('title',['type'=>'text','class'=>'form-control','label'=>false,'required'=>false]) ?>
    </div> 
	<div class="form-group">
        <div id="wrapper-avatar-upload">
            <button type="button" class="btn buttonImgBlog btn-info"><label style="font-weight: 400;" for="fileAvatarUpload" id="labelImageblog">Thiết lập hình đại diện tin</label></button>
            <div id="image-holder">
                <img class="thumb-image img-thumbnail"></img>
            </div>
            <?php echo $this->Form->control("avatar", array("style"=>"display:none;","type" => "file", "accept" => "image/png, image/jpeg, image/gif", "id" => "fileAvatarUpload", "label" => false)) ?>
        </div>
	</div> 
	<div class="form-group">
		<label class="control-label">Tóm tắt</label>
		<?php echo $this->Form->textarea('describes',['class'=>'form-control','placeholder'=>'Mô tả ngắn','label'=>false, 'rows'=>"3"]) ?>
	</div>  	
	<?= $this->Html->script('ckeditor/ckeditor') ?>
    <div class="form-group">
        <label class="control-label">Nội dung bài viết</label>
        <?php echo $this->Form->textarea('content',['class'=>'ckeditor form-control','placeholder'=>'Nội dung','label'=>false, 'rows'=>"15"]) ?>
    </div>
    <div class="form-group">
        <label for="status" class="control-label">Trạng thái: </label>
        <?php echo $this->Form->radio('status',['Ẩn','Hiển thị'],['legend' => false,'default'=>1]) ?>
    </div>
	<div class="form-group">
        <button type="submit" class="btn btn-primary">Lưu</button>
        <button type="button" class="btn btn-primary back">Hủy</button>
    </div> 
<?= $this->Form->end() ?>

<script>
	$(document).ready(function() {
        $(".back").click(function(){
            if(confirm("Chưa tạo tin bạn có muốn quay về trang danh sách tin")){ 
                window.location.href = "/admin/blogs/list_blogs";
            }
        });

	});

    $("#fileAvatarUpload").on('change', function () {
        var countFiles = $(this)[0].files.length;
        var filesize = this.files[0].size;
        
        var imgPath = $(this)[0].value;
        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        if (countFiles > 1) {
            alert('Chỉ chọn một ảnh.');
        } else if(countFiles === 0){
            $("#wrapper-avatar-upload img.thumb-image" ).attr("src","");
            $("#wrapper-avatar-upload img.thumb-image" ).attr("title",'Bạn cần chọn ảnh làm hình đại diện');
        } else {
            if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                if(filesize > 524288){
                    alert("Yêu cầu chọn ảnh có dung lượng không quá 512Kb.");
                }                
                if (typeof (FileReader) != "undefined") {
                    if(filesize <= 524288){
                        for (var i = 0; i < countFiles; i++)
                        {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $("#wrapper-avatar-upload img.thumb-image" ).attr("src",e.target.result);
                                $("#wrapper-avatar-upload img.thumb-image" ).attr("title",'Hình đại diện cho tin.');
                            }
                            $("#image-holder").css("display","block");
                            reader.readAsDataURL($(this)[0].files[i]);
                        }
                    } 
                } else {
                    alert("File không được hỗ trợ.");
                }
            } else {
                alert("File không được hỗ trợ.");
            }
        }
    });

    //select multiple
    $(document).ready(function() {
        let branch_all = [];
        
        function formatResult(state) {
            branch_all.push(state.id);
            var id = 'state' + state.id;
            var checkbox = $('<div class="checkbox"><input id="'+id+'" type="checkbox" '+(state.selected ? 'checked': '')+'><label for="checkbox1">'+state.text+'</label></div>', { id: id });
            return checkbox;   
        }
        
        function arr_diff(a1, a2) {
            var a = [], diff = [];
            for (var i = 0; i < a1.length; i++) {
                a[a1[i]] = true;
            }
            for (var i = 0; i < a2.length; i++) {
                if (a[a2[i]]) {
                    delete a[a2[i]];
                } else {
                    a[a2[i]] = true;
                }
            }
            for (var k in a) {
                diff.push(k);
            }
            return diff;
        }
        
        let optionSelect2 = {
            templateResult: formatResult,
            closeOnSelect: false,
            width: '100%'
        };
        
        let $select2 = $("#country").select2(optionSelect2);
        
        var scrollTop;
        
        $select2.on("select2:selecting", function( event ){
            var $pr = $( '#'+event.params.args.data._resultId ).parent();
            scrollTop = $pr.prop('scrollTop');
        });
        
        $select2.on("select2:select", function( event ){
            $(window).scroll();
            
            var $pr = $( '#'+event.params.data._resultId ).parent();
            $pr.prop('scrollTop', scrollTop );
            
            $(this).val().map(function(index) {
                $("#state"+index).prop('checked', true);
            });
        });
        
        $select2.on("select2:unselecting", function ( event ) {
            var $pr = $( '#'+event.params.args.data._resultId ).parent();
            scrollTop = $pr.prop('scrollTop');
        });
        
        $select2.on("select2:unselect", function ( event ) {
            $(window).scroll();
            
            var $pr = $( '#'+event.params.data._resultId ).parent();
            $pr.prop('scrollTop', scrollTop );
            
            var branch  =   $(this).val() ? $(this).val() : [];
            var branch_diff = arr_diff(branch_all, branch);
            branch_diff.map(function(index) {
                $("#state"+index).prop('checked', false);
            });
        });
    });              

</script>
<style type="text/css" media="screen">
    .img-thumbnail {
        display: inline-block;
        /*max-width: 200px;*/
        width: 190px;
        height: 190px;
        padding: 4px;
        line-height: 1.42857143;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        transition: all .2s ease-in-out;
    }
    #labelImageblog{
        margin-bottom: 0px;
    } 
    .buttonImgBlog{
        margin-bottom: 5px;
    }
    #image-holder{
        display: none;
    }
    .select2-results__option {
        padding: 1px 0px !important;
    } 
    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        position: absolute;
        margin-top: 4px\9;
        margin-left: 0px;
    }
    .checkbox input[type=checkbox] {
        display: none;
    }    
</style>