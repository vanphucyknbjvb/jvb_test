<div class="form-group">   
  <h2>Danh sách tin tức</h2>
</div>  
<?php $session = $this->request->getSession(); ?>
<div class="form-group"> 
  <?= $this->Flash->render() ?>
</div>  
<div class="form-group">
    <a href="/blogs/add_blog"><button class="btn btn-primary">Thêm tin mới</button></a>
</div>
<div class="form-group row">
  <?php echo $this->Form->create(); ?>
  <div class="col-md-3 col-sm-5 col-xs-6">
    <?php echo $this->Form->control('key_word',['class'=>'form-control','placeholder'=>'nhập tên sản phẩm cần tìm kiếm','label'=>false,'value'=>($session->check('keyWordListBlogs')) ? $session->read('keyWordListBlogs') : '']); ?>
  </div>
  <div class="col-md-3 col-sm-3 col-xs-3 row">
    <?php echo $this->Form->control('Tìm',['type'=>'submit','class'=>'btn btn-primary buttonSearch']) ?>
  </div>
  <?php echo $this->Form->end(); ?>  
</div>  
<table class="table table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col" style="width: 24%;">Tiêu đề</th>
      <th scope="col" style="width: 45%;">Mô tả</th>
      <th scope="col" style="width: 8%;min-width: 100px;">Trạng thái</th>
      <th scope="col" style="width: 13%;">Ngày tạo</th>
      <th scope="col" style="width: 10%;min-width: 130px;">Actions</th>
    </tr>
  </thead>
  <tbody>
  	<?php foreach($listblogs as $blog) { ?>
	    <tr>
	      	<td><a href="/news/detail/<?= $blog['id'] ?>" target="_blank" class="link_a" title="Xem"><?php echo h($this->Text->truncate($blog['title'],60)); ?></a></td>
        	<td><?php echo h($this->Text->truncate($blog['describes'],100)); ?></td>
        	<td><?php echo (h($blog['status'] == 1)) ? 'Hiển thị' : 'Ẩn'; ?></td>
        	<td><?php echo date_format($blog['created'],"Y/m/d G:i:s"); ?></td>
	      	<td style="text-align: center;">
          		<a href="/blogs/edit-blog/<?= $blog['id'] ?>"><button class="btn btn-primary" type="button">sửa</button></a>
          		<?php echo $this->Form->postLink("xóa","/blogs/delete-blog/".$blog['id'],["confirm"=>"Bạn thực sự có muốn xóa tin: ".$blog['title'],'class'=>'btn btn-danger']); ?>
        	</td>
	    </tr>
    <?php } ?>
  </tbody>
</table>

<div class="text-center">
  <ul class="pagination">
    <?php echo $this->Paginator->prev(' < ' . __('Trang sau')); ?>
    <?php echo $this->Paginator->numbers(array('separator' => ' ')); ?>
    <?php echo $this->Paginator->next( __('Trang tiếp').' > '); ?>    
  </ul>
</div>