<?php echo $this->Html->css(['select2.min.css']) ?>
<?php echo $this->Html->script('select2.min.js') ?>
<h2>Thêm Tin tức</h2><br />
<?= $this->Flash->render() ?>
<?= $this->Form->create($blog,['type'=>'file']) ?>
<?php $this->Form->setTemplates(['inputContainer' => '{{content}}']); ?>
    <div class="form-group">
        <label class="control-label">Tiêu đề<span class="required">*</span></label>
        <?php echo $this->Form->control('title',['type'=>'text','class'=>'form-control','label'=>false,'required'=>false]) ?>
    </div>
	<div class="form-group">
        <div id="wrapper-avatar-upload">
            <button type="button" class="btn buttonImgBlog btn-info"><label style="font-weight: 400;" for="avatar" id="labelImageblog">Thiết lập hình đại diện tin</label></button>
            <button type="button" class="btn buttonImgBlog image-preview-clear" style="display:none;">
                <i class="fa fa-times"></i>Giữ ảnh ban đầu
            </button>
            <?php empty($blog['url_image']) ? $display='none' : $display='block'; ?>            
            <div id="image-holder" style="display: <?php echo $display;?>">
                <img id="preview_avatar" class="thumb-image img-thumbnail" <?php if(!empty($blog['url_image'])): ?> src= <?php echo h($blog['url_image']); endif; ?> ></img>
            </div>
            <span class="input-group-btn" style="display: none;">
                <div class="btn btn-default image-preview-input">
                    <span class="glyphicon glyphicon-folder-open"></span>
                    <?= $this->Form->control('avatar', array(
                        'type' => 'file',
                        'label' => false,
                        'option' => array(
                            'accept' => 'image/png, image/jpeg, image/gif'
                        )
                    ))?>
                </div>
            </span>
        </div>
	</div> 
	<div class="form-group">
		<label class="control-label">Tóm tắt</label>
		<?php echo $this->Form->textarea('describes',['class'=>'form-control','placeholder'=>'Mô tả ngắn','label'=>false, 'rows'=>"3"]) ?>
	</div>    	
	<?= $this->Html->script('ckeditor/ckeditor') ?>
    <div class="form-group">
        <label class="control-label">Nội dung bài viết</label>
        <?php echo $this->Form->textarea('content',['class'=>'ckeditor form-control','placeholder'=>'Nội dung','label'=>false, 'rows'=>"15"]) ?>
    </div>
    <div class="form-group">
        <label for="status" class="control-label">Trạng thái: </label>
        <?php echo $this->Form->radio('status',['Ẩn','Hiển thị']) ?>
    </div>
	<div class="form-group">
        <button type="submit" class="btn btn-primary">Lưu</button>
        <button type="button" class="btn btn-primary back">Hủy</button>
    </div> 
<?= $this->Form->end() ?>

<script>
	$(document).ready(function() {
        $(".back").click(function(){
            if(confirm("Chưa tạo tin bạn có muốn quay về trang danh sách tin")){ 
                window.location.href = "/blogs/list_blogs";
            }
        });
	});

    var srcImg = $('#preview_avatar').attr('src');
    $(".image-preview-input input:file").change(function (){
        var countFiles = $(this)[0].files.length;
        var filesize = this.files[0].size;
        var imgPath = $(this)[0].value;
        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        if (countFiles > 1) {
            alert('Chỉ chọn một ảnh.');
        } else if(countFiles === 0){
            alert("Chưa chọn ảnh.");
        } else {
            if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                if(filesize > 524288){
                    alert("Yêu cầu chọn ảnh có dung lượng không quá 512Kb.");
                }else{            	
	                var img = $('#preview_avatar');
	                var file = this.files[0];
	                var reader = new FileReader();
	                reader.onload = function (e) {
	                    $(".image-preview-clear").show();
	                    img.attr('src', e.target.result);
	                }
	                reader.readAsDataURL(file);
                    $("#image-holder").css("display","block");
                }             	
            } else {
                alert("File không được hỗ trợ.");
            }
        }
        
    });
    // Clear event
    $('.image-preview-clear').click(function(){
        $(".image-preview-clear").hide();
        $('.image-preview-input input:file').val('');
        var img = $('#preview_avatar');
        img.attr('src', srcImg);
    }); 

    //select multiple
    $(document).ready(function() {
        let branch_all = [];
        
        function formatResult(state) {
            branch_all.push(state.id);
            var id = 'state' + state.id;
            var checkbox = $('<div class="checkbox"><input id="'+id+'" type="checkbox" '+(state.selected ? 'checked': '')+'><label for="checkbox1">'+state.text+'</label></div>', { id: id });
            return checkbox;   
        }
        
        function arr_diff(a1, a2) {
            var a = [], diff = [];
            for (var i = 0; i < a1.length; i++) {
                a[a1[i]] = true;
            }
            for (var i = 0; i < a2.length; i++) {
                if (a[a2[i]]) {
                    delete a[a2[i]];
                } else {
                    a[a2[i]] = true;
                }
            }
            for (var k in a) {
                diff.push(k);
            }
            return diff;
        }
        
        let optionSelect2 = {
            templateResult: formatResult,
            closeOnSelect: false,
            width: '100%'
        };
        
        let $select2 = $("#country").select2(optionSelect2);
        
        var scrollTop;
        
        $select2.on("select2:selecting", function( event ){
            var $pr = $( '#'+event.params.args.data._resultId ).parent();
            scrollTop = $pr.prop('scrollTop');
        });
        
        $select2.on("select2:select", function( event ){
            $(window).scroll();
            
            var $pr = $( '#'+event.params.data._resultId ).parent();
            $pr.prop('scrollTop', scrollTop );
            
            $(this).val().map(function(index) {
                $("#state"+index).prop('checked', true);
            });
        });
        
        $select2.on("select2:unselecting", function ( event ) {
            var $pr = $( '#'+event.params.args.data._resultId ).parent();
            scrollTop = $pr.prop('scrollTop');
        });
        
        $select2.on("select2:unselect", function ( event ) {
            $(window).scroll();
            
            var $pr = $( '#'+event.params.data._resultId ).parent();
            $pr.prop('scrollTop', scrollTop );
            
            var branch  =   $(this).val() ? $(this).val() : [];
            var branch_diff = arr_diff(branch_all, branch);
            branch_diff.map(function(index) {
                $("#state"+index).prop('checked', false);
            });
        });
    });      

</script>
<style type="text/css" media="screen">
    .img-thumbnail {
        display: inline-block;
        /*max-width: 200px;*/
        width: 190px;
        height: 190px;
        padding: 4px;
        line-height: 1.42857143;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        transition: all .2s ease-in-out;
    }
    #labelImageblog{
        margin-bottom: 0px;
    } 
    .buttonImgBlog{
        margin-bottom: 5px;
    }
    .select2-results__option {
        padding: 1px 0px !important;
    } 
    .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
        position: absolute;
        margin-top: 4px\9;
        margin-left: 0px;
    }
    .checkbox input[type=checkbox] {
        display: none;
    }           
</style>