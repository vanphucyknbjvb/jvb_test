<?php

namespace App\Controller;

use Cake\Event\Event;
use Cake\Database\Connection;
use Cake\Error\ErrorHandler;
use Cake\Routing\Router;

class UsersController extends AppController
{  
    public function initialize() {
        parent::initialize();
    }
    
    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('username hoặc password không thành công, thử lại nhé!'));
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }        

}
