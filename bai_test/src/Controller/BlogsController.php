<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Core\Exception\Exception;
use Cake\Filesystem\File;
use Cake\Utility\Text;

class BlogsController extends AppController
{

    public function initialize() {
        parent::initialize();

        $this->loadComponent('Flash');      
       
    }

    public function addBlog(){
        $this->viewBuilder()->setLayout('blog');
    	$blogTable = TableRegistry::get('Blog');
    	if($this->request->is('POST')){
    		$data = $this->request->getData();
            $saveBlog = $blogTable->newEntity($data);
            if(empty($saveBlog->getErrors())){
                $img = $this->uploadPublicFile($data['avatar'],'/upload/imagesBlogs/');
                if($img['error']==FALSE){ $saveBlog->url_image = $img['path']; }
                if($blogTable->save($saveBlog)){ 
                    $this->Flash->success('Thêm tin tức thành công');
                    $this->redirect(['action' => 'listBlogs']);
                }else{
                    $this->Flash->error('Lỗi thêm tin tức');
                }
            }else{
                $this->Flash->error('Lỗi thêm tin tức');
                $this->set('saveBlog',$saveBlog);
            }
    	}
    }

    public function uploadPublicFile($file,$path,$allowMaxSize=2,$allowExt = array('jpg', 'jpeg', 'gif', 'png')){
        $result = array(
            'error'         => true,
            'errorMessage'  => 'Upload file không thành công!',
            'path'          => ''
        );
        if(isset($file['size'])){
                if($file['size'] > $allowMaxSize*1024*1024){
                $result['errorMessage'] = 'Vượt quá dung lượng cho phép!';
                return $result;
            }
        }
        if(isset($file['name'])){
            $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
        }
       
        if(in_array($ext,$allowExt)){
            $newNameFile = $path.Text::uuid() . '_' . time() . '_' . rand(00000, 99999).'.'.$ext;
            try{
                if(move_uploaded_file($file['tmp_name'], WWW_ROOT . $newNameFile)){
                    $result['error'] = false;
                    $result['path']  = $newNameFile;
                    $result['errorMessage'] = 'Upload file thành công';
                }
            }catch (Exception $e){
                pd($e->getMessage());
            }
        }else{
            $result['errorMessage'] = 'Không đúng định dạng file!';
        }

        return $result;
    }    

    public function listBlogs(){
        $this->viewBuilder()->setLayout('blog');
        $blogTable = TableRegistry::get('Blog');
        $session = $this->request->getSession();
        $condition = array('deleted !=' => 1);
        if($this->request->is('POST')){
            $data = $this->request->getData();
            $session->write('keyWordListBlogs',trim($data['key_word']));
            $condition = array('title like' => '%'.trim($data['key_word']).'%','deleted !=' => 1);
        }
        if($session->check('keyWordListBlogs')){
            $keyWord = $session->read('keyWordListBlogs');
            $condition = array('title like' => '%'.$keyWord.'%','deleted !=' => 1);
        }                
        $blogs = $blogTable->find('all')->where($condition);
        $paginate = [
            'order' => [
                'modified' => 'desc'
            ],
            'limit' => 10
        ];      
        $listblogs = $this->paginate($blogs,$paginate);
        $this->set('listblogs',$listblogs);
    }

    public function deleteBlog($id){
        $blogTable = TableRegistry::get('Blog');
        if($this->request->is('POST')){
            if($blogTable->updateAll(['deleted' => 1],['id' => $id])){
                $this->Flash->success('Xóa tin tức thành công');
            }else{                
                $this->Flash->error('Lỗi xóa xóa tin tức');
            } 
        }
        return $this->redirect(['action'=>'listBlogs']);
    }

    public function editBlog($id){
        $this->viewBuilder()->setLayout('blog');
        $blogTable = TableRegistry::get('Blog');
        $blog = $blogTable->findById($id)->where(['deleted !=' => 1])->first();
        if(!$blog){return $this->redirect(['controller' => 'Blogs', 'action' => 'listBlogs']);}
        if($this->request->is(['POST','PUT'])){
            $data = $this->request->getData(); 
            $blog = $blogTable->get($id);
            $blogTable->patchEntity($blog,$data);
            if(empty($blog->getErrors())){
                if($data['avatar']['name']){
                    $img = $this->uploadPublicFile($data['avatar'],'/upload/imagesBlogs/');
                    if($img['error']==FALSE){ 
                        $fileOld = new File(WWW_ROOT . $blog['url_image']);
                        $fileOld->delete();                        
                        $blog->url_image = $img['path'];  
                    }
                }
                if($blogTable->save($blog)){
                    $this->Flash->success('Sửa tin tức thành công');
                    $this->redirect(['controller' => 'Blogs', 'action' => 'listBlogs']);
                }                    
            }else{
                $this->Flash->error('Lỗi sửa tin tức');
            }   
        }
        $this->set('blog',$blog);
    }

}    