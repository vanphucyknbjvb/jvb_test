<?php

namespace App\Libs;

use App\Libs\BaseShinseiFormItemLib;

class ShinseiFormHiddenLib extends BaseShinseiFormItemLib {

    function renderForm($values = array()) {
        $result = parent::renderForm($values);

        $keys = array_keys($values);
        $key = $keys[0];

        $result .= $this->_Form->hidden(
                sprintf("%s.%s.svalue", $this->_model_name, $key)
                , array('value' => $values[$key]['svalue'])
        );

        return $result;
    }

}
