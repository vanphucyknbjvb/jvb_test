<?php

namespace App\Libs;

use App\Libs\BaseShinseiFormItemLib;

class ShinseiFormCheckboxLib extends BaseShinseiFormItemLib {

    function renderForm($values = array()) {
        $result = parent::renderForm($values);

        $keys = array_keys($values);
        $key = $keys[0];
        $skey = $values[$key]['skey'];

        $checked = ( $this->_item_config[$skey]['form_options']['value'] == $values[$key]['svalue'] );
        $form_options = $this->_item_config[$skey]['form_options'] + array('hiddenFields' => true, 'checked' => $checked)
        ;

        $result .= $this->_Form->checkbox(
                sprintf("%s.%s.svalue", $this->_model_name, $key)
                , $form_options
        );

        $for_id = sprintf('%s%sSvalue', ucwords($this->_model_name), ucwords($key));

        $result = sprintf('%s<label for="%s"> %s</label>'
                , $result
                , $for_id
                , $this->_item_config[$skey]['options']['comment']
        );

        return $result;
    }

    function render($values = array()) {
        $keys = array_keys($values);
        $key = $keys[0];
        $skey = $values[$key]['skey'];
        $svalue = $values[$key]['svalue'];

        $c = $this->_item_config[$skey]['options']['comment'];

        if ($svalue) {
            return "■" . $c;
        } else {
            return "□" . $c;
        }
    }

}
