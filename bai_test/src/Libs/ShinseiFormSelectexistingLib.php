<?php

namespace App\Libs;


use App\Libs\BaseShinseiFormItemLib;
use Cake\ORM\TableRegistry;

class ShinseiFormSelectexistingLib extends BaseShinseiFormItemLib {

    function renderForm($values = array()) {
        $result = parent::renderForm($values);
        $keys = array_keys($values);
        $key = $keys[0];
        $skey = $values[$key]['skey'];

        $form_options = $this->_item_config[$skey]['form_options'] + array('multiple' => false, 'empty' => true, 'onchange' => 'select_existing_change(this);')
        ;
        $existing = $this->_item_config[$skey]['options']['existing'];

        $vals = array();
        if ($existing == 'departments') {
            $vals = $this->getDepartmentsList($values[$key]['svalue']);
        } else if ($existing == 'belongs') {
            $vals = $this->getBelongsList($values[$key]['svalue']);
        } else if ($existing == 'jobs') {
            $vals = $this->getJobsList($values[$key]['svalue']);
        }
        $options = $this->options_key_value($vals);

        $val = $values[$key]['svalue'];
//print($val);
        if (!in_array($val, $vals) && !empty($val)) {
            $val = "その他";
        }
        $form_options['value'] = $val;
        $form_options['id'] = $this->_model_name.$key.'Svalue';
        $result .= $this->_Form->select(
                sprintf("%s.%s.svalue", $this->_model_name, $key)
                , $options
                , $form_options
        );

        $style = ( $val == 'その他' ) ? null : "display:none;";
        $v = ( $values[$key]['svalue'] == 'その他' ) ? null : $values[$key]['svalue'];
        
//print_a($vals);    
        $result .= "　" . $this->_Form->text(
                        sprintf("%s.%s.remarks", $this->_model_name, $key)
                        , $this->_item_config[$skey]['form_options'] + array('value' => $v, 'class' => 'remark', 'style' => $style)
        );

        return $result;
    }

    function getDepartmentsList($current_name = null) {
        //App::import('Model', 'Department');
        $departmentTbl = TableRegistry::get('Department');

        //診療科セレクト情報作成START
        $depCond = array('Department.enable' => 9);
        $depOrd = array('Department.view_number' => 'asc');
        $departments = array();
        $dataChk = true;

        // $departmentClass = new Department();
        // $departmentClass->contain();

        $data = $departmentTbl->find('all', array('fields' => array('name'), 'order' => $depOrd, 'conditions' => $depCond));
        foreach ($data as $key => $value) {
            $departments[$value['name']] = $value['name'];
            if (strcmp($value, $current_name) == 0) {
                $dataChk = false;
            }
        }
        if ($dataChk && !empty($current_name)) {
//        $departments[$current_name] = $current_name;
        }
        return $departments;
    }

    function getBelongsList($current_name = null) {
        $belongTbl = TableRegistry::get('Belong');
        //App::import('Model', 'Belong');

        //診療科セレクト情報作成START
        $belongCond = array('Belong.enable' => 9);
        $belongOrd = array('Belong.view_number' => 'asc');
        $belongs = array();
        $dataChk = true;

        // $belongClass = new Belong();
        // $belongClass->contain();

        $data = $belongTbl->find('all', array('fields' => array('name'), 'order' => $belongOrd, 'conditions' => $belongCond));
        
        foreach ($data as $key => $value) {
            $belongs[$value['name']] = $value['name'];
            if (strcmp($value, $current_name) == 0) {
                $dataChk = false;
            }
        }
        if ($dataChk && !empty($current_name)) {
//        $belongs[$current_name] = $current_name;
        }
        return $belongs;
    }

    function getJobsList($current_name = null) {
        // App::import('Model', 'Job');
        $jobTbl = TableRegistry::get('Job');

        //診療科セレクト情報作成START
        $jobCond = array('Job.enable' => 9);
        $jobOrd = array('Job.view_number' => 'asc');
        $jobs = array();
        $dataChk = true;

        // $jobClass = new Job();
        // $jobClass->contain();

        $data = $jobTbl->find('all', array('fields' => array('name'), 'order' => $jobOrd, 'conditions' => $jobCond));
        foreach ($data as $key => $value) {
            $jobs[$value['name']] = $value['name'];
            if (strcmp($value, $current_name) == 0) {
                $dataChk = false;
            }
        }
        if ($dataChk && !empty($current_name)) {
//        $jobs[$current_name] = $current_name;
        }
        return $jobs;
    }

}
