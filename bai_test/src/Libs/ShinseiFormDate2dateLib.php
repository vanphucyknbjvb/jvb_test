<?php

namespace App\Libs;

use App\Libs\BaseShinseiFormItemLib;
use App\Libs\ShinseiFormDateLib;
use App\Libs\ShinseiFormHiddenLib;

class ShinseiFormDate2dateLib extends BaseShinseiFormItemLib {

    function renderForm($values = array()) {
        $result = parent::renderForm($values);

        $dateform = new ShinseiFormDateLib($this->_model_name, $this->_item_config);

        $value = $this->findBySkey('from', $values);
        $result .= $dateform->renderForm($value);

        $result .= " ～ ";

        $value = $this->findBySkey('to', $values);
        $result .= $dateform->renderForm($value);

        $hiddenform = new ShinseiFormHiddenLib($this->_model_name, $this->_item_config);

        $value = $this->findBySkey('nosub', $values);
        $result .= $hiddenform->renderForm($value);

        return $result;
    }

    function render($values = array()) {
        $value = $this->findBySkey('from', $values, false);
        $from = isset($value['svalue']) ? $value['svalue'] : null;

        $value = $this->findBySkey('to', $values, false);
        $to = $value['svalue'];

        return sprintf("%s ～ %s", $this->datecb($from), $this->datecb($to));
    }

    protected function datecb($v) {
        if ($v) {
            $dt = date_create($v);
            return date_format($dt, 'Y年n月j日');
//      return date('Y年n月j日', strtotime($v));
        } else {
            return "　年　月　日";
        }
    }

}
