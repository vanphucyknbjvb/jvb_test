<?php

namespace App\Libs;

use Cake\View\Helper\HtmlHelper;
use Cake\View\Helper\FormHelper;

class BaseShinseiFormItemLib {

    protected $_Form = null;
    protected $_model_name = null;
    protected $_item_config = null;
    protected $_is_project = null;

    function __construct($model_name, $item_config, $is_project = null) {
        $this->_Form = new FormHelper(new \Cake\View\View());
        $this->_Form->Html = new HtmlHelper(new \Cake\View\View());

        $this->_model_name = $model_name;
        $this->_item_config = $item_config;
        $this->_is_project = $is_project;
    }

    /**
      values example

      $values[0][id]
      $values[0][shoshiki_name]
      $values[0][ssection]
      :
      $values[3][id]
      $values[3][shoshiki_name]
      $values[3][ssection]

      valuesの添え字は、ssectionが同一のもののみ抽出されている
      なので0,3,6などのように飛び順になっていてもよい

      ssectionが異なるものは$valuesに与えてはならない
     */
    function renderForm($values = array()) {
        $result = null;
        $hf = array("id", "shinsei_id", "project_shinsei_id", "shoshiki_name", "ssection", "skey");

        foreach ($values as $k => $v) {
            foreach ($hf as $f) {

                if (empty($v[$f])) {
                    $result .= $this->_Form->hidden(sprintf("%s.%s.%s", $this->_model_name, $k, $f), array('value' => null));
                } else {
                    $result .= $this->_Form->hidden(sprintf("%s.%s.%s", $this->_model_name, $k, $f), array('value' => $v[$f]));
                }
            }
        }

        return $result;
    }

    function render($values = array()) {
        $result = null;

        foreach ($values as $k => $v) {
            $result .= $v['svalue'];
        }

        return $result;
    }

    function renderTitle($values = array()) {
        $result = null;
        foreach ($values as $k => $v) {
            $result .= $this->_item_config[$v['skey']]['title'];
        }
        return $result;
    }

    function findBySkey($skey, $values, $withkey = true) {
        foreach ($values as $k => $v) {
            if ($v['skey'] === $skey) {
                if ($withkey) {
                    return array($k => $v);
                } else {
                    return $v;
                }
            }
        }
        return array();
    }

    function findBySkeyOccur($skey, $occur, $values, $withkey = true) {
        foreach ($values as $k => $v) {
            if ($v['skey'] === $skey && $v['occurs'] === $occur) {
                if ($withkey) {
                    return array($k => $v);
                } else {
                    return $v;
                }
            }
        }
        return array();
    }

    function getSelectDate() {
        $r_y = array();
        $r_m = array();
        $r_d = array();

        $nowYear = date('Y');

        for ($y = $nowYear - 120; $y <= $nowYear + 36; $y++) {
            $r_y[$y] = $y;
        }

        for ($i = 1; $i < 13; $i++) {
            $cnt = substr('00' . $i, -2, 2);
            $r_m[$cnt] = $i;
        }
        for ($j = 1; $j < 32; $j++) {
            $cnt = substr('00' . $j, -2, 2);
            $r_d[$cnt] = $j;
        }
        return array('year' => $r_y, 'month' => $r_m, 'date' => $r_d);
    }

    function options_key_value($options) {
        // value を key にする
        $r = array();

        foreach ($options as $k => $v) {
            $r[$v] = $v;
        }
        return $r;
    }

}
