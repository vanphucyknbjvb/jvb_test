<?php

namespace App\Libs;

use App\Libs\BaseShinseiFormItemLib;

class ShinseiFormDateLib extends BaseShinseiFormItemLib {

    function renderForm($values = array()) {
        $result = parent::renderForm($values);

        $keys = array_keys($values);
        $key = $keys[0];
        $skey = $values[$key]['skey'];

        $options = $this->_item_config[$skey]['options'];
        $from_text = "";
        if (!empty($options['from_text'])) {
            $from_text = $options['from_text'];
        }
        $ymd = $this->getSelectDate();
        $v = explode("-", $values[$key]['svalue']);
        $form_options = array('empty' => true);

        $result .= '<div style="display:inline;">';
        $result .= $from_text;
        $result .= $this->_Form->select(
                sprintf("%s.%s.svalue.year", $this->_model_name, $key)
                , $ymd['year']
                , ($form_options + array('value' =>  isset($v[0]) ? $v[0] : null))
        );
        $result .= "年";

        $result .= $this->_Form->select(
                sprintf("%s.%s.svalue.month", $this->_model_name, $key)
                , $ymd['month']
                ,  ($form_options + array('value' => isset($v[1]) ? $v[1] : null))
        );
        $result .= "月";

        $result .= $this->_Form->select(
                sprintf("%s.%s.svalue.date", $this->_model_name, $key)
                , $ymd['date']
                ,  ($form_options + array('value' =>  isset($v[2]) ? $v[2] : null))
        );
        $result .= "日";

        $result .= '<input type="hidden" class="cal" />';
        $result .= '</div>';

        if (!empty($this->_item_config[$skey]['options']['left_remarks'])) {
            $result = $this->_item_config[$skey]['options']['left_remarks'] . $result;
        }
        if (!empty($this->_item_config[$skey]['options']['right_remarks'])) {
            $result = $result . $this->_item_config[$skey]['options']['right_remarks'];
        }
        return $result;
    }

    function render($values = array()) {
        $v = parent::render($values);

        $keys = array_keys($values);
        $key = $keys[0];
        $skey = $values[$key]['skey'];

        $options = $this->_item_config[$skey]['options'];
        $from_text = "";
        if (!empty($options['from_text'])) {
            $from_text = $options['from_text'];
        }

        $av = explode("-", $v);
        $r = $from_text;

        if (isset($av[0]) && $av[0]) {
            $r .= sprintf('%s年', $av[0]);
        } else {
            $r .= "　年";
        }

        if (isset($av[1]) && $av[1]) {
            $r .= sprintf('%s月', $av[1]);
        } else {
            $r .= "　月";
        }

        if (isset($av[2]) && $av[2]) {
            $r .= sprintf('%s日', $av[2]);
        } else {
            $r .= "　日";
        }

        return $r;
    }

}
