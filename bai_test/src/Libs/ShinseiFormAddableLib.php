<?php

namespace App\Libs;

use App\Libs\BaseShinseiFormItemLib;
use Cake\Core\Configure;

class ShinseiFormAddableLib extends BaseShinseiFormItemLib {

    function renderForm($values = array()) {
        $result = parent::renderForm($values);

        $result .= '<div class="AddableForm">';

        $result .= $this->_render('renderForm', $values);

        $result .= "</div>";

        $value = $this->findBySkey('nosub', $values);
        $nosub_key = array_keys($value);
        $nosub_key = $nosub_key[0];

        //$result .= '<div class="main_02"><div class="btn_01"><a href="#" onclick="add();return false;">行追加</a></div></div>';
        $result .= '<div class="main_02"><a href="#" onclick="add(\'' . $nosub_key . '\');return false;"><img src="/img2/button/add_row3.jpg" /></a></div>';
        $result .= '<script>setupAddableForm()</script>';

        return $result;
    }

    function render($values = array()) {
        $forms = array_keys($this->_item_config);
        $occurs = $this->_item_config['nosub']['options']['occur'];

        $nv = $this->findBySkey('nosub', $values);

        for ($i = 0; $i < $occurs; $i++) {
            $v = array();
            $c = "";
            foreach ($forms as $fk => $f) {
                $cv = $this->findBySkeyOccur($f, (string) $i, $values);
                if ($cv) {
                    $v = array_merge($v, $cv);
                    $k = array_keys($cv);
                    $k = $k[0];
                    $c .= $cv[$k]['svalue'];
                }
            }

            if ($c != "") {
                $nv = array_merge($nv, $v);
            }
        }

        return $this->_render('render', $nv);
    }

    function _render($method, $values = array()) {
        $result = "";

        $value = $this->findBySkey('nosub', $values);
        $nosub_key = array_keys($value);
        $nosub_key = $nosub_key[0];

        $views = $this->_item_config['nosub']['options']['view'];
        $forms = array_keys($this->_item_config);
        $occurs = $this->_item_config['nosub']['options']['occur'];

        $j = 0;
        for ($i = 0; $i < $occurs; $i++) {
//      $result .= sprintf('<table id="TableForAddableForm_%s" class="table_10 table_11" width="90%%">', $i);
            $result .= sprintf('<table id="%s_%s" class="table_10 table_11" width="90%%">', $nosub_key, $i);
            foreach ($forms as $fk => $f) {
                if ($f == "nosub") {
                    continue;
                }

                $cv = $this->findBySkeyOccur($f, (string) $i, $values);
                if (empty($cv)) {
                    $cv = $this->findBySkeyOccur($f, null, $values, false);
                    if (empty($cv)) {
                        continue;
                    }
                    $cv = array(uniqid() => $cv);
                }
                $even = '';
                $j++;
                if ($j % 2 == 0)
                    $even = 'even';
                else
                    $even = '';
                $result .= "<tr class='" . $even . "'>";

                $t = Configure::read(sprintf('shinsei.%s.items.%s.%s.type'
                                        , $value[$nosub_key]['shoshiki_name'], $value[$nosub_key]['ssection'], $f));
                
                $class_name = "App\Libs\ShinseiForm" . ucwords($t).'Lib';

                $o = new $class_name($this->_model_name, $this->_item_config);

                $ck = array_keys($cv);
                $ck = $ck[0];
                $result .= $this->_Form->hidden(sprintf("%s.%s.occurs", $this->_model_name, $ck), array('value' => $i));
                
                $border_none = '';
                if ($i > 0) {
                    $border_none = 'style="border-top: none;"';
                }

                $result .= sprintf('<th width="20%%" %s>%s %s)</th><td %s>%s</td>'
                        , $border_none
                        , call_user_func(array($o, 'renderTitle'), $cv)
                        , $i + 1
                        , $border_none
                        , call_user_func(array($o, $method), $cv)
                );

                $result .= "</tr>";
            }
            $result .= "</table>";
        }

        return $result;
    }

}
