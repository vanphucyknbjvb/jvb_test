<?php

namespace App\Libs;

use App\Libs\BaseShinseiFormItemLib;
use Cake\Core\Configure;

class ShinseiFormRadiowithLib extends BaseShinseiFormItemLib {

    function renderForm($values = array()) {
        $result = parent::renderForm($values);

        $value = $this->findBySkey('nosub', $values);
        $nosub_key = array_keys($value);
        $nosub_key = $nosub_key[0];
        $svalue = explode('|', $value[$nosub_key]['svalue']);
        $check_values = $this->_item_config['nosub']['options']['options'];

        $form_options = $this->_item_config['nosub']['form_options'];
        if(!isset($form_options['class'])){
            $form_options['class'] = '';
        }
        if(!isset($form_options['onclick'])){
            $form_options['onclick'] = '';
        }

        foreach ($check_values as $k => $v) {
            $checked = in_array((string) $v, $svalue, true);
            $name = sprintf("%s[%s][svalue][]", $this->_model_name, $nosub_key);
            $id = sprintf("%s%sSvalue%s", ucwords($this->_model_name), ucwords($nosub_key), ucwords($k));

            $result .= '<div class="checkbox" style="vartical-align:middle">';
            $result .= sprintf('<input type="radio" id="%s" value="%s" %s name="%s" style="vertical-align:center" class="%s" onclick="%s"><label for="%s"  style="vertical-align:center">%s</label>'
                    , $id
                    , $v
                    , $checked ? 'checked="checked"' : ''
                    , $name
                    , $form_options['class']
                    , $form_options['onclick']
                    , $id
                    , $v
            );

            // 子フォーム
            $cv = $this->findBySkey("key" . $k, $values);
            if (empty($cv)) {
                continue;
            }
            $t = Configure::read(sprintf('shinsei.%s.items.%s.key%s.type', $value[$nosub_key]['shoshiki_name'], $value[$nosub_key]['ssection'], $k));

            $class_name = "App\Libs\ShinseiForm" . ucwords($t).'Lib';
            
            $o = new $class_name($this->_model_name, $this->_item_config);

            $result .= " " . $o->renderForm($cv);
            $result .= "</div>\n";
        }
        return $result;
    }

    function render($values = array()) {
        $result = "";

        $value = $this->findBySkey('nosub', $values);
        $nosub_key = array_keys($value);
        $nosub_key = $nosub_key[0];
        $svalue = explode('|', $value[$nosub_key]['svalue']);
        $check_values = $this->_item_config['nosub']['options']['options'];

        foreach ($check_values as $k => $v) {
            $checked = in_array((string) $v, $svalue, true);
            if ($checked) {
                $result .= $v;

                $cv = $this->findBySkey("key" . $k, $values);
                if (!empty($cv)) {
                    $t = Configure::read(sprintf('shinsei.%s.items.%s.key%s.type', $value[$nosub_key]['shoshiki_name'], $value[$nosub_key]['ssection'], $k));
                    
                    $class_name = "App\Libs\ShinseiForm" . ucwords($t).'Lib';
                    
                    $o = new $class_name($this->_model_name, $this->_item_config);

                    $result .= sprintf("(%s)", $o->render($cv));
                }

                $result .= "<br />";
            }
        }
        return $result;
    }

}
