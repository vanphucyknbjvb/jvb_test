<?php

namespace App\Libs;

use App\Libs\BaseShinseiFormItemLib;

class ShinseiFormRadioLib extends BaseShinseiFormItemLib {

    function renderForm($values = array()) {
        $result = parent::renderForm($values);
        $keys = array_keys($values);
        $key = $keys[0];
        $skey = $values[$key]['skey'];

        $options = $this->radio_key_value($this->_item_config[$skey]['options']['options']);
        $form_options = $this->_item_config[$skey]['form_options'] + array(
            'label' => false, 
            'options' => $options, 
            'type' => 'radio',
            'value' => $values[$key]['svalue'])
        ;
        
        if(isset($this->_item_config[$skey]['form_options']['separator']) && 
                preg_replace('/\s+/', '', $this->_item_config[$skey]['form_options']['separator']) == '<br/>') {
            $form_options['templates']['radioWrapper'] = '<div class="radio-block screen-center screen-radio">{{label}}</div><br/>';
        } else {
             $form_options['templates']['radioWrapper'] = '<div class="radio-block screen-center screen-radio">{{label}}</div>　';
        }
        $form_options['templates']['radioFormGroup'] = '{{input}}{{label}}';

        if (isset($this->_item_config[$skey]['options']['comment'])) {
            $result .= sprintf("<b>%s</b><br />", $this->_item_config[$skey]['options']['comment']);
        }
        $this->_Form->setTemplates([
            'radioContainer' => '<div class="form-radio">{{content}}</div>'
        ]);
        $result .= $this->_Form->input(
                sprintf("%s.%s.svalue", $this->_model_name, $key)
                , $form_options
        );
        return $result;
    }

    function render($values = array()) {
        $result = parent::render($values);

        $keys = array_keys($values);
        $key = $keys[0];
        $skey = $values[$key]['skey'];

        if (isset($this->_item_config[$skey]['options']['comment'])) {
            $result = sprintf("<b>%s</b><br />", $this->_item_config[$skey]['options']['comment']) . $result;
        }

        return $result;
    }
    
    function radio_key_value($options) {

        
        // value を key にする
        $r = array();

        foreach ($options as $k => $v) {
            $r[$k-1]['value'] = $v;
            $r[$k-1]['text'] = $v;
            //$r[$v] = $v;
        }

        return $r;
    }

}
