<?php

namespace App\Libs;

use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\Core\Configure;
use App\Libs\ShinseiFormLib;
use Cake\View\View;
use App\Controller\AppController;

/**
 * Created by linhtnk<n.khanhlinh@jvb-corp.com>.
 * Date: 11/05/2018
 * Time: 03:05 PM
 */
class ShinseiFormRendererLib {

    protected $_shoshikiName = null;
    protected $_values = null;
    protected $_forms = null;
    protected $_shinseiDetail = null;
    protected $_itemViewConfig = null;
    protected $_shinseiId = null;
    protected $_isProject = null;
    protected $_isEdit = null;
    protected $_examInfo = null;

    function __construct($is_project, $shinsei_id, $board_group_id, $shinsei_type, $shoshiki_name, $shoshiki_view_page = null, $exam_info = null) {

        $this->_shoshikiName = $shoshiki_name;

        if ($is_project) {
            $config_name = 'project_shinsei';
        } else {
            $config_name = 'shinsei';
        }

        if (!is_null($shoshiki_view_page)) {
            // 編集のきは、ページごとに表示するので、該当ページ分のみ設定情報を取得する
            $this->_itemViewConfig = Configure::read(sprintf("%s.%s.items_view.%s", $config_name, $this->_shoshikiName, $shoshiki_view_page));
            $this->_isEdit = 1;
        } else {
            // 詳細表示のときなどはpage番号指定はなしで全情報表示するべきなので、設定情報をページをまたいでマージする
            $this->_itemViewConfig = array();
            $tmp_itemViewConfig = Configure::read(sprintf("%s.%s.items_view", $config_name, $this->_shoshikiName));
            if (!empty($tmp_itemViewConfig)) {
                foreach ($tmp_itemViewConfig as $p => $vr) {
                    $this->_itemViewConfig = array_merge($this->_itemViewConfig, $vr);
                    $this->_isEdit = 0;
                }
            }
        }

        /*
          if ( !$this->_itemViewConfig)
          {
          throw new ShinseiFormRendererException(sprintf("No item_view configuration with shoshiki_name=%s", $this->_shoshikiName));
          }
         */

        $this->_shinseiId = $shinsei_id;
        $this->_isProject = $is_project;
        $this->_examInfo = $exam_info;
    }

    /**
      $this->data をそのまま引数にする
      shoshiki_name, ssection, skey, occursでソートされていること
     */
    function assign($values) {
        $this->_forms = array();

        foreach ($this->_itemViewConfig as $row => $row_value) {
            foreach ($row_value as $col => $item) {
                $sf = new ShinseiFormLib($this->_isProject, $this->_shinseiId, $this->_shoshikiName, $item);

                // item_view のこの行列位置の項目名(ssection)が
                // 引数$valuesの中にないか確認し、あればそのデータをShinseiFormに渡す
                foreach ($values as $k => $v) {
                    if ($v['shoshiki_name'] == $this->_shoshikiName && $v['ssection'] == $item) {
                        $sf->add($k, $v);
                    }
                }

                $this->_forms[$row][$col] = $sf;
            }
        }

        //書式用の配列を作成する
        $this->create_shoshiki_array($values);
    }

    /**
      書式用の配列を作成
     */
    function create_shoshiki_array($shinsei_details) {
        $this->_shinseiDetail = array();
        $i = 0;
        $before_key_name = '';
        $occor_count = 1;

        //書式用に要素名を修正
        foreach ($shinsei_details as $row => $shinsei_detail) {
            $arr_key = $shinsei_detail['ssection'] . "_";
            $arr_key .= $shinsei_detail['skey'];
            if (!empty($shinsei_detail['occurs'])) {
                if ($before_key_name == $arr_key) {
                    $occor_count++;
                    $before_key_name = $arr_key;
                } else {
                    $occor_count = 1;
                    $before_key_name = $arr_key;
                }
                $arr_key = $arr_key . "_" . $occor_count;
            }
            $this->_shinseiDetail[$arr_key] = $shinsei_details[$i];
            $i++;
        }

        //初回登録用の試験基本情報を作成
        $exam_info_cols = array(
            array('name' => 'project_num', 'occurs' => null),
            array('name' => 'exam_num', 'occurs' => null),
            array('name' => 'division', 'occurs' => null),
            array('name' => 'target', 'occurs' => null),
            array('name' => 'hospital', 'occurs' => 2),
            array('name' => 'exam_drug_name', 'occurs' => null),
            array('name' => 'exam_plan_num', 'occurs' => null),
            array('name' => 'exam_name', 'occurs' => null),
            array('name' => 'kenkyu_jigyo', 'occurs' => null),
            array('name' => 'kenkyu_jigyo_remarks', 'occurs' => null),
            array('name' => 'board_name', 'occurs' => null),
            array('name' => 'board_chairman', 'occurs' => null),
            array('name' => 'enforcement_start_date', 'occurs' => null),
            array('name' => 'enforcement_end_date', 'occurs' => null),
            array('name' => 'jissi_rei', 'occurs' => null),
            array('name' => 'mokuhyou_rei', 'occurs' => null),
            array('name' => 'doctor_name', 'occurs' => null),
            array('name' => 'doctor_job', 'occurs' => null),
            array('name' => 'doctor_belong_name', 'occurs' => null),
            array('name' => 'department_name', 'occurs' => null),
            array('name' => 'shounin_no', 'occurs' => null),
        );

        if (!empty($this->_examInfo)) {
            foreach ($exam_info_cols as $exam_info_col) {
                if (!isset($this->_examInfo[$exam_info_col['name']])) {
                    continue;
                }

                $id = null;
                //すでに存在する場合はidを取得
                if (array_key_exists('ShinseiDetail', $this->_shinseiDetail)) {
                    if (array_key_exists($exam_info_col['name'] . '_nosub', $this->_shinseiDetail)) {
                        if (!empty($this->_shinseiDetail[$exam_info_col['name'] . '_nosub'])) {
                            $id = $this->_shinseiDetail[$exam_info_col['name'] . '_nosub']['id'];
                            $val = $this->_shinseiDetail[$exam_info_col['name'] . '_nosub']['svalue'];
                        }
                    }
                }
                //配列を作成
                $add_shinsei = array();
                $add_shinsei['id'] = $id;
                if ($this->_isProject) {
                    $add_shinsei['project_shinseiId'] = $this->_shinseiId;
                    $add_shinsei['shinsei_id'] = null;
                } else {
                    $add_shinsei['project_shinseiId'] = null;
                    $add_shinsei['shinsei_id'] = $this->_shinseiId;
                }
                $add_shinsei['shoshiki_name'] = $this->_shoshikiName;
                $add_shinsei['ssection'] = $exam_info_col['name'];
                $add_shinsei['svalue'] = (isset($val)) ? $val : $this->_examInfo[$exam_info_col['name']];
                $add_shinsei['skey'] = 'nosub';
                $add_shinsei['deleted'] = 0;
                $this->_shinseiDetail[$exam_info_col['name'] . '_nosub'] = $add_shinsei;

                if (!is_null($exam_info_col['occurs'])) {
                    for ($i = 1; $i <= $exam_info_col['occurs']; $i++) {
                        $add_shinsei['occurs'] = $i;
                        $this->_shinseiDetail[$exam_info_col['name'] . '_nosub_' . $i] = $add_shinsei;
                    }
                }
            }
        }
        //print_a($this->_shinseiDetail);
    }

    function renderForm() {
        return $this->_render('renderForm');
    }

    function render() {
        return $this->_render('render', false, false);
    }

    protected function _render($render_method, $show_comment = true, $show_required = true) {
        $result = null;
        // 最大列数を取得しておいて、すべての行でその列分だけ表示する
        $max_col_count = 1;
        foreach ($this->_forms as $row => $row_value) {
            if (count($row_value) > $max_col_count) {
                $max_col_count = count($row_value);
            }
        }
        $i = 0;
        foreach ($this->_forms as $row => $row_value) {
            $i++;
            $even = '';
            if ($i % 2 != 0)
                $even = 'even';
            else
                $even = '';
            $result .= "<tr class='" . $even . "'>\n";
            $widtht = 10;
            if ($max_col_count == 1) {
                $widtht = 20;
            }
            $width = ceil(( 100 - $widtht * $max_col_count) / $max_col_count);
            $colspan = 1;
            if (( $max_col_count > 1) && ( count($row_value) == 1)) {
                $colspan = $max_col_count * 2 - 1;
            }
            foreach ($row_value as $k => $value) {
                $colvalue = call_user_func(array($value, $render_method));

                if (!is_null($colvalue)) {
                    $required = null;
                    if ($show_required) {
                        $required = '<span class="font_yl"> ※必須</span>';
                        if (!$value->isRequired()) {
                            $required = null;
                        }
                    }

                    $comment = null;
                    if ($show_comment) {
                        $comment = $value->renderComment();
                        if ($comment) {
                            $comment = sprintf('<span class="font_green">%s</span>', $comment);
                        }
                    }

                    $upper_comment = null;
                    if ($show_comment) {
                        $upper_comment = $value->renderUpperComment();
                        if ($upper_comment) {
                            $upper_comment = sprintf('<span class="font_green">%s</span><br/>', $upper_comment);
                        }
                    }

                    if ($value->renderTitle()) {
                        $result .= sprintf('<th width="%s%%" class="w_l_02"><strong>%s</strong>%s</th><td width="%s%%" colspan="%s"><p>%s</p>%s<p>%s</p></td>' . "\n"
                                , ($k == 1) ? $widtht : 5
                                , $value->renderTitle()
                                , $required
                                , $width
                                , $colspan
                                , $upper_comment
                                , $colvalue
                                , $comment
                        );
                    } else {
                        $result .= sprintf('<td width="%s%%" class="w_l_02" colspan="%s"><p>%s</p>%s<p>%s</p></td>' . "\n"
                                , (($k == 1) ? $widtht : 5) + $width
                                , $colspan + 1
                                , $upper_comment
                                , $colvalue
                                , $comment
                        );
                    }
                } else {
                    // 書式ビュー
                    $shoshiki = $value->renderPath();
                    if (!empty($shoshiki)) {

                        ob_start();
                        $view = new View();
                        $form = $view->Form;
                        $shoshikiutil = $view->Shoshikiutil;
                        $date = $view->Date;
                        $dateex = $view->Dateex;
                        $html = $view->Html;
                        
//                        App::import('Helper', 'Html');
//                        App::import('Helper', 'Form');
//                        App::import('Helper', 'Shoshikiutil');
//                        App::import('Controller', 'AppController');

//                        $form = new FormHelper();
//                        $html = new HtmlHelper();
//                        $shoshikiutil = new ShoshikiutilHelper();
//                        $date = new DateHelper();
//                        $dateex = new DateexHelper();
//                        $form->Html = $html;

                        $app_controller = new AppController();
                        $examOutputCommonDivision = $app_controller->examOutputCommonDivision;
                        $examOutputCommonTarget = $app_controller->examOutputCommonTarget;

                        $selectYear2 = $app_controller->getSelectYear();
                        $selectMonth = $app_controller->getSelectMonth();
                        $selectDate = $app_controller->getSelectDay();
                        $selectBirthYear = $app_controller->getSelectBirthYear();

                        if ($this->_isProject) {
                            $project_shinseiId = $this->_shinseiId;
                            $shinsei_id = null;
                        } else {
                            $project_shinseiId = null;
                            $shinsei_id = $this->_shinseiId;
                        }

                        $shoshiki_name = $this->_shoshikiName;
                        $shinsei = $this->_shinseiDetail;

                        if ($this->_isEdit) {
                            $filename = $shoshiki['edit_view'];
                        } else {
                            $filename = $shoshiki['detail_view'];
                        }

                        require_once(dirname(__FILE__) . '/views/' . $filename);
                        $contents = ob_get_clean();
                        $result .= $contents;
                    } else {
                        // label
                        $width2 = $widtht + $width;
                        $result .= sprintf('<td width="%s%%" colspan="%s" style="background-color:#CCFF99;"><strong>%s</strong></td>' . "\n"
                                , $width2
                                , $max_col_count * 2
                                , $value->renderTitle());
                    }
                }
            }

            $result .= "</tr>\n";
        }
        return $result;
    }

}
