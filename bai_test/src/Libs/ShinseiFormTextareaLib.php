<?php

namespace App\Libs;

use App\Libs\BaseShinseiFormItemLib;

class ShinseiFormTextareaLib extends BaseShinseiFormItemLib {

    function renderForm($values = array()) {
        $result = parent::renderForm($values);

        $keys = array_keys($values);
        $key = $keys[0];
        $skey = $values[$key]['skey'];

        $onkeyup = null;
        if (isset($this->_item_config[$skey]['options']['maxlength'])) {
            $maxlength = $this->_item_config[$skey]['options']['maxlength'];
            $onkeyup = sprintf("textareaInputLimit(this,%s);", $maxlength);
        }

        $result .= $this->_Form->textarea(
                sprintf("%s.%s.svalue", $this->_model_name, $key)
                , $this->_item_config[$skey]['form_options'] + array('value' => $values[$key]['svalue'], 'style' => 'vertical-align:baseline;', 'onkeyup' => $onkeyup)
        );

        return $result;
    }

    function render($values = array()) {
        return nl2br(parent::render($values));
    }

}
