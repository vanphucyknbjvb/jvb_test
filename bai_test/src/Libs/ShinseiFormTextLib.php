<?php

namespace App\Libs;

use App\Libs\BaseShinseiFormItemLib;

class ShinseiFormTextLib extends BaseShinseiFormItemLib {

    function renderForm($values = array()) {
        $result = parent::renderForm($values);

        $keys = array_keys($values);
        $key = $keys[0];
        $skey = $values[$key]['skey'];

        $result .= $this->_Form->text(
                sprintf("%s.%s.svalue", $this->_model_name, $key)
                , $this->_item_config[$skey]['form_options'] + array('value' => $values[$key]['svalue'])
        );

        if (!empty($this->_item_config[$skey]['options']['left_remarks'])) {
            $result = $this->_item_config[$skey]['options']['left_remarks'] . $result;
        }
        if (!empty($this->_item_config[$skey]['options']['right_remarks'])) {
            $result = $result . $this->_item_config[$skey]['options']['right_remarks'];
        }
        return $result;
    }

}
