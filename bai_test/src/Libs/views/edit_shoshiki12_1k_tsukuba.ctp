<link rel="stylesheet" type="text/css" href="/css/style_formatOutput.css" />
<script type="text/javascript" src="/js/movealert.js"></script>

<script type="text/javascript">

    function checkOnlyOneBox(target) {
        var checkBoxClass = $(target).attr('class');
        if ($(target).attr('checked') == true) {
            $('.' + checkBoxClass).attr('checked', false);
            $(target).attr('checked', true);
        }
    }

    function delete_change_contents(id) {

        var change = "ShinseiDetail";
        var number = get_detail_number(id);

        $("#" + change + "YugaiJisyoNameNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoNameNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoKichiMichiNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoKichiMichiNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoHatsugenDateNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoHatsugenDateNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoDieNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoDieNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoDieOsoreNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoDieOsoreNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoNyuinNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoNyuinNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoSyogaiNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoSyogaiNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoSyogaiOsoreNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoSyogaiOsoreNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoJyutokuNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoJyutokuNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoSentenNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoSentenNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoTenkiDateNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoTenkiDateNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoTenkiRecoverNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoTenkiRecoverNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoTenkiKeikaiNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoTenkiKeikaiNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoTenkiMikaihukuNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoTenkiMikaihukuNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoTenkiKoisyoNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoTenkiKoisyoNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoTenkiDieNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoTenkiDieNosub" + number + "Deleted").val(1);

        $("#" + change + "YugaiJisyoTenkiUnknownNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "YugaiJisyoTenkiUnknownNosub" + number + "Deleted").val(1);

        $("#" + change + "DrugToyoStartDateNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "DrugToyoStartDateNosub" + number + "Deleted").val(1);

        $("#" + change + "DrugToyoIsEndNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "DrugToyoIsEndNosub" + number + "Deleted").val(1);

        $("#" + change + "DrugToyoEndDateNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "DrugToyoEndDateNosub" + number + "Deleted").val(1);

        $("#" + change + "DrugToyoIsToyoChuNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "DrugToyoIsToyoChuNosub" + number + "Deleted").val(1);

        $("#" + change + "DrugIngaHiteiDekinaiNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "DrugIngaHiteiDekinaiNosub" + number + "Deleted").val(1);

        $("#" + change + "DrugIngaHiteiDekiruNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "DrugIngaHiteiDekiruNosub" + number + "Deleted").val(1);

        $("#" + change + "DrugIngaUnknownNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "DrugIngaUnknownNosub" + number + "Deleted").val(1);

        $("#" + change + "DrugSochiChushiNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "DrugSochiChushiNosub" + number + "Deleted").val(1);

        $("#" + change + "DrugSochiNoChangeNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "DrugSochiNoChangeNosub" + number + "Deleted").val(1);

        $("#" + change + "DrugSochiUnknownNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "DrugSochiUnknownNosub" + number + "Deleted").val(1);

        $("#" + change + "DrugSochiGaitosezuNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "DrugSochiGaitosezuNosub" + number + "Deleted").val(1);

        $("#" + change + "DrugSochiGenryoNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "DrugSochiGenryoNosub" + number + "Deleted").val(1);

        $("#" + change + "DrugSochiZouryoNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "DrugSochiZouryoNosub" + number + "Deleted").val(1);

        $("#" + change + "DrugSochiYohoYoryoNosub" + number + "Occur").attr({class: ''});
        $("#" + change + "DrugSochiYohoYoryoNosub" + number + "Deleted").val(1);

        $("#" + id).css('display', 'none');

        return false;
    }

    function add_jyutoku_contents(added_id)
    {
        // 追加されるIDをnumに保持
        var num = $("#" + added_id).val();

        // クローンを取得
        row_data = $('#jutokuContent_1').clone(false);

        // クローン行のIDを変更
//  $(row_data).attr('id','jyutokuContent_'+num);

        $(row_data).attr('id', 'ShinseiDetailYugaiJisyoNameNosub1Id');



        // クローン内のTDタグを検索
        // TDタグ内にある、textarea、input、deleteLink の各タグについて、重複させないようname値を変更する。
        $('table td', row_data).each(function () {

            $('hidden', this).each(function () {
                $(this).attr('name', $(this).attr("name").replace(/(\d+)/g, num)).val("");
                $(this).attr('id', $(this).attr("id").replace(/(\d+)/g, num)).val("");
            });
            $('textarea', this).each(function () {
                $(this).attr('name', $(this).attr("name").replace(/(\d+)/g, num)).val("");
                $(this).attr('id', $(this).attr("id").replace(/(\d+)/g, num)).val("");
            });
            $('input:not(:button,:radio,:checkbox)', this).each(function () {
                $(this).attr('name', $(this).attr("name").replace(/(\d+)/g, num)).val("");
            });
            $('input:checkbox', this).each(function () {
                $(this).attr('name', $(this).attr("name").replace(/(\d+)/g, num)).attr('checked', false);
            });
            $('input:radio', this).each(function () {
                $(this).attr('name', $(this).attr("name").replace(/(\d+)/g, num)).attr('checked', false);
            });
            $('select', this).each(function () {
                $(this).attr('name', $(this).attr("name").replace(/(\d+)/g, num)).val("");
            });

            $('input.cal', this).each(function () {
                $(this).removeAttr('id');
                $(this).attr('class', 'cal');
            });
            $('div .ui-datepicker-trigger', this).each(function () {
                $(this).remove();
            });
        });

        $('.deleteLink', row_data).each(function () {
            $(this).attr('style', 'display:inline');
            $(this).removeAttr('onclick');
            $(this).click(function () {
                return delete_jyutoku_row('jyutokuContent_' + num);
            });
        });

        // 追加ボタンの前に挿入
        $("#jutoku_add_row").before(row_data);

        setupCalendar(2010, 2015, '/img/icon_ca.jpg');

        // 次の項番を保持
        $("#" + added_id).val(num * 1 + 1);
        // occurを正しく再設定。
        sortNumberJyutoku();

        return false;
    }

</script> 

<?php 
    $form->templates(['inputContainer' => '{{content}}']);
?>
<div id="container">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_03" id="infoArea">
        <td width="15" colspan="2" style="background-color:#CCFF99;"><strong>重篤な有害事象及び不具合に関する報告書</strong></td>
    </table>

    <div id="fmt_wrapper">
        <div id="fmt_header">
            <div id="fmt_header_left">
                （研究実施責任者→研究機関の長）医療機器用
            </div>

            <div id="fmt_header_right">
                <!-- 申請日 -->
                <?php echo $form->hidden('ShinseiDetail.shinsei_date_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'shinsei_date_nosub', 'id'))); ?>
                <?php echo $form->hidden('ShinseiDetail.shinsei_date_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                <?php echo $form->hidden('ShinseiDetail.shinsei_date_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                <?php echo $form->hidden('ShinseiDetail.shinsei_date_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                <?php echo $form->hidden('ShinseiDetail.shinsei_date_nosub.ssection', array('value' => 'shinsei_date')); ?>
                <?php echo $form->hidden('ShinseiDetail.shinsei_date_nosub.skey', array('value' => 'nosub')); ?>
                <?php
                if (!empty($shinsei['shinsei_date_nosub']['svalue'])) {
                    if (!is_array($shinsei['shinsei_date_nosub']['svalue'])) {
                        $date = explode('-', $shinsei['shinsei_date_nosub']['svalue']);
                        $shinsei['shinsei_date_nosub'] = array();
                        $shinsei['shinsei_date_nosub']['year'] = $date[0];
                        $shinsei['shinsei_date_nosub']['month'] = $date[1];
                        $shinsei['shinsei_date_nosub']['day'] = $date[2];
                    }
                } else {
                    $shinsei['shinsei_date_nosub'] = array();
                    $shinsei['shinsei_date_nosub']['year'] = null;
                    $shinsei['shinsei_date_nosub']['month'] = null;
                    $shinsei['shinsei_date_nosub']['day'] = null;
                }
                ?>
                <div>
                    <?php echo $form->select('ShinseiDetail.shinsei_date_nosub.year', $selectYear2, array('empty' => true, 'value' => $shinsei['shinsei_date_nosub']['year'])) ?>
                    年
                    <?php echo $form->select('ShinseiDetail.shinsei_date_nosub.month', $selectMonth, array('empty' => true, 'value' => $shinsei['shinsei_date_nosub']['month'])) ?>
                    月
                    <?php echo $form->select('ShinseiDetail.shinsei_date_nosub.day', $selectDate, array('empty' => true, 'value' => $shinsei['shinsei_date_nosub']['day'])) ?>
                    日 <input type="hidden" class="cal" />
                </div>
            </div>
        </div>

        <div id="fmt_title">
            重篤な有害事象に関する報告書（第
            <?php echo $form->hidden('ShinseiDetail.common_hou_num_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'common_hou_num_nosub', 'id'))); ?>
            <?php echo $form->hidden('ShinseiDetail.common_hou_num_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
            <?php echo $form->hidden('ShinseiDetail.common_hou_num_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
            <?php echo $form->hidden('ShinseiDetail.common_hou_num_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
            <?php echo $form->hidden('ShinseiDetail.common_hou_num_nosub.ssection', array('value' => 'common_hou_num')); ?>
            <?php echo $form->hidden('ShinseiDetail.common_hou_num_nosub.skey', array('value' => 'nosub')); ?>
            <?php echo $form->input('ShinseiDetail.common_hou_num_nosub.svalue', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'common_hou_num_nosub', 'svalue'))); ?>
            報）
        </div>

        <div class="fmt_contents">

            <div class="fmt_left fmt_fill">
                <span class="fmt_label_uline">研究機関の長</span><br />
                <span class="fmt_label_right">
                    <?php echo $form->hidden('ShinseiDetail.hospital_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'hospital_nosub', 'id'))); ?>
                    <?php echo $form->hidden('ShinseiDetail.hospital_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                    <?php echo $form->hidden('ShinseiDetail.hospital_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                    <?php echo $form->hidden('ShinseiDetail.hospital_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                    <?php echo $form->hidden('ShinseiDetail.hospital_nosub.ssection', array('value' => 'hospital')); ?>
                    <?php echo $form->hidden('ShinseiDetail.hospital_nosub.skey', array('value' => 'nosub')); ?>
                    <?php echo $form->hidden('ShinseiDetail.hospital_nosub.svalue', array('value' => $shoshikiutil->value($shinsei, 'hospital_nosub', 'svalue'))); ?>
                    <?php echo $shoshikiutil->value($shinsei, 'hospital_nosub', 'svalue') ?>
                </span><br />

                <?php echo $form->hidden('ShinseiDetail.hospital_chief_job_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'hospital_chief_job_nosub', 'id'))); ?>
                <?php echo $form->hidden('ShinseiDetail.hospital_chief_job_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                <?php echo $form->hidden('ShinseiDetail.hospital_chief_job_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                <?php echo $form->hidden('ShinseiDetail.hospital_chief_job_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                <?php echo $form->hidden('ShinseiDetail.hospital_chief_job_nosub.ssection', array('value' => 'hospital_chief_job')); ?>
                <?php echo $form->hidden('ShinseiDetail.hospital_chief_job_nosub.skey', array('value' => 'nosub')); ?>
                <?php echo $form->input('ShinseiDetail.hospital_chief_job_nosub.svalue', array('type' => 'text', 'size' => '30', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'hospital_chief_job_nosub', 'svalue'))); ?>殿
            </div>

            <div class="fmt_right">
                <span class="fmt_label_uline">研究実施責任者</span><br />
                （氏名）
                <?php echo $form->hidden('ShinseiDetail.doctor_name_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'doctor_name_nosub', 'id'))); ?>
                <?php echo $form->hidden('ShinseiDetail.doctor_name_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                <?php echo $form->hidden('ShinseiDetail.doctor_name_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                <?php echo $form->hidden('ShinseiDetail.doctor_name_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                <?php echo $form->hidden('ShinseiDetail.doctor_name_nosub.ssection', array('value' => 'doctor_name')); ?>
                <?php echo $form->hidden('ShinseiDetail.doctor_name_nosub.skey', array('value' => 'nosub')); ?>
                <?php echo $form->input('ShinseiDetail.doctor_name_nosub.svalue', array('type' => 'text', 'size' => '30', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'doctor_name_nosub', 'svalue'))); ?>
            </div>

            <div class="fmt_left fmt_caption fmt_clear">
                下記の臨床研究等において、以下のとおり重篤と判断される有害事象及び不具合等を認めたので報告いたします。
            </div>

            <div class="fmt_label_center fmt_caption fmt_clear">
                記
            </div>

            <table class="table_fmt" cellspacing=0 cellpadding=0 border="0" width="100%">
                <tr>
                    <th class="fmt_label_width_long">研究課題名</th>
                    <td colspan="3">
                        <?php echo $form->hidden('ShinseiDetail.exam_name_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'exam_name_nosub', 'id'))); ?>
                        <?php echo $form->hidden('ShinseiDetail.exam_name_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                        <?php echo $form->hidden('ShinseiDetail.exam_name_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                        <?php echo $form->hidden('ShinseiDetail.exam_name_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                        <?php echo $form->hidden('ShinseiDetail.exam_name_nosub.ssection', array('value' => 'exam_name')); ?>
                        <?php echo $form->hidden('ShinseiDetail.exam_name_nosub.skey', array('value' => 'nosub')); ?>
                        <?php echo $form->hidden('ShinseiDetail.exam_name_nosub.svalue', array('value' => $shoshikiutil->value($shinsei, 'exam_name_nosub', 'svalue'))); ?>
                        <?php echo $shoshikiutil->value($shinsei, 'exam_name_nosub', 'svalue') ?>
                    </td>
                </tr>
                <tr>
                    <th class="fmt_label_width_long">
                        臨床研究登録ID
                    </th>
                    <td>
                        <?php echo $form->hidden('ShinseiDetail.rinsho_db_id_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'rinsho_db_id_nosub', 'id'))); ?>
                        <?php echo $form->hidden('ShinseiDetail.rinsho_db_id_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                        <?php echo $form->hidden('ShinseiDetail.rinsho_db_id_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                        <?php echo $form->hidden('ShinseiDetail.rinsho_db_id_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                        <?php echo $form->hidden('ShinseiDetail.rinsho_db_id_nosub.ssection', array('value' => 'rinsho_db_id')); ?>
                        <?php echo $form->hidden('ShinseiDetail.rinsho_db_id_nosub.skey', array('value' => 'nosub')); ?>
                        <?php echo $form->input('ShinseiDetail.rinsho_db_id_nosub.svalue', array('type' => 'text', 'size' => '40', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'rinsho_db_id_nosub', 'svalue'))); ?>
                    </td>
                </tr>
            </table>
            <br/>

            <table class="table_fmt" cellspacing=0 cellpadding=0 border="0" width="100%">
                <tr>
                    <th class="fmt_label_width_long">
                        発生機関
                    </th>
                    <td>
                        <div>
                            <?php echo $form->hidden('ShinseiDetail.hassei_kikan_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'hassei_kikan_nosub', 'id'))); ?>
                            <?php echo $form->hidden('ShinseiDetail.hassei_kikan_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                            <?php echo $form->hidden('ShinseiDetail.hassei_kikan_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                            <?php echo $form->hidden('ShinseiDetail.hassei_kikan_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                            <?php echo $form->hidden('ShinseiDetail.hassei_kikan_nosub.ssection', array('value' => 'hassei_kikan')); ?>
                            <?php echo $form->hidden('ShinseiDetail.hassei_kikan_nosub.skey', array('value' => 'nosub')); ?>
                            <?php
                            echo $form->input('ShinseiDetail.hassei_kikan_nosub.svalue', array('type' => 'radio', 'options' => array('1' => '自施設', '2' => '他の共同研究機関'),
                                'separator' => '　', 'legend' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'hassei_kikan_nosub', 'svalue'), 'label' => false))
                            ?>

                            <?php echo $form->hidden('ShinseiDetail.hassei_kikan_remarks_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'hassei_kikan_remarks_nosub', 'id'))); ?>
                            <?php echo $form->hidden('ShinseiDetail.hassei_kikan_remarks_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                            <?php echo $form->hidden('ShinseiDetail.hassei_kikan_remarks_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                            <?php echo $form->hidden('ShinseiDetail.hassei_kikan_remarks_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                            <?php echo $form->hidden('ShinseiDetail.hassei_kikan_remarks_nosub.ssection', array('value' => 'hassei_kikan_remarks')); ?>
                            <?php echo $form->hidden('ShinseiDetail.hassei_kikan_remarks_nosub.skey', array('value' => 'nosub')); ?>
                            （機関名：<?php echo $form->input('ShinseiDetail.hassei_kikan_remarks_nosub.svalue', array('type' => 'text', 'size' => '40', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'hassei_kikan_remarks_nosub', 'svalue'))); ?>）
                        </div>
                        <div style="font-size:smaller;">＊他の共同研究機関の場合は、以下共同研究機関からの報告書類の添付も可</div>
                    </td>
                </tr>
                <tr>
                    <th class="fmt_label_width_long">
                        識別コード<br />（自施設の場合）
                    </th>
                    <td>
                        <?php echo $form->hidden('ShinseiDetail.patient_code_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'patient_code_nosub', 'id'))); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_code_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_code_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_code_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_code_nosub.ssection', array('value' => 'patient_code')); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_code_nosub.skey', array('value' => 'nosub')); ?>
                        <?php echo $form->input('ShinseiDetail.patient_code_nosub.svalue', array('type' => 'text', 'size' => '40', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'patient_code_nosub', 'svalue'))); ?>
                    </td>
                </tr>
            </table>
            <br/>


            <div class="fmt_category_title">重篤な有害事象発現者の情報</div>
            <table class="table_fmt" cellspacing=0 cellpadding=0 border="0" width="100%">
                <tr>
                    <td rowspan="2" width="23%">
                        重篤な有害事象発現者の区分<br />
                        <?php echo $form->hidden('ShinseiDetail.patient_div_is_patient_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'patient_div_is_patient_nosub', 'id'))); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_is_patient_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_is_patient_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_is_patient_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_is_patient_nosub.ssection', array('value' => 'patient_div_is_patient')); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_is_patient_nosub.skey', array('value' => 'nosub')); ?>
                        <?php echo $form->checkbox('ShinseiDetail.patient_div_is_patient_nosub.svalue', array('class' => 'patient_div', 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'patient_div_is_patient_nosub', 'svalue'), 'label' => false, 'div' => false)) ?>被験者<br />

                        <?php echo $form->hidden('ShinseiDetail.patient_div_other_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'patient_div_other_nosub', 'id'))); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_other_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_other_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_other_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_other_nosub.ssection', array('value' => 'patient_div_other')); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_other_nosub.skey', array('value' => 'nosub')); ?>
                        <?php echo $form->checkbox('ShinseiDetail.patient_div_other_nosub.svalue', array('class' => 'patient_div', 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'patient_div_other_nosub', 'svalue'), 'label' => false, 'div' => false)) ?>その他<br />

                        <?php echo $form->hidden('ShinseiDetail.patient_div_other_remarks_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'patient_div_other_remarks_nosub', 'id'))); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_other_remarks_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_other_remarks_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_other_remarks_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_other_remarks_nosub.ssection', array('value' => 'patient_div_other_remarks')); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_div_other_remarks_nosub.skey', array('value' => 'nosub')); ?>
                        （<?php echo $form->input('ShinseiDetail.patient_div_other_remarks_nosub.svalue', array('type' => 'text', 'size' => '20', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'patient_div_other_remarks_nosub', 'svalue'))); ?>）
                    </td>

                    <td width="17%">
                        体重：　
                        <?php echo $form->hidden('ShinseiDetail.patient_weight_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'patient_weight_nosub', 'id'))); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_weight_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_weight_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_weight_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_weight_nosub.ssection', array('value' => 'patient_weight')); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_weight_nosub.skey', array('value' => 'nosub')); ?>
                        <?php echo $form->input('ShinseiDetail.patient_weight_nosub.svalue', array('type' => 'text', 'size' => '5', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'patient_weight_nosub', 'svalue'))); ?> kg<br />
                        身長：　
                        <?php echo $form->hidden('ShinseiDetail.patient_height_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'patient_height_nosub', 'id'))); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_height_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_height_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_height_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_height_nosub.ssection', array('value' => 'patient_height')); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_height_nosub.skey', array('value' => 'nosub')); ?>
                        <?php echo $form->input('ShinseiDetail.patient_height_nosub.svalue', array('type' => 'text', 'size' => '5', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'patient_height_nosub', 'svalue'))); ?> cm
                    </td>
                    <td width="30%">

                        <?php echo $form->hidden('ShinseiDetail.patient_birthday_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'patient_birthday_nosub', 'id'))); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_birthday_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_birthday_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_birthday_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_birthday_nosub.ssection', array('value' => 'patient_birthday')); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_birthday_nosub.skey', array('value' => 'nosub')); ?>
                        <?php
                        if (!empty($shinsei['patient_birthday_nosub']['svalue'])) {
                            if (!is_array($shinsei['patient_birthday_nosub']['svalue'])) {
                                $date = explode('-', $shinsei['patient_birthday_nosub']['svalue']);
                                $shinsei['patient_birthday_nosub'] = array();
                                $shinsei['patient_birthday_nosub']['year'] = $date[0];
                                $shinsei['patient_birthday_nosub']['month'] = $date[1];
                                $shinsei['patient_birthday_nosub']['day'] = $date[2];
                            }
                        } else {
                            $shinsei['patient_birthday_nosub'] = array();
                            $shinsei['patient_birthday_nosub']['year'] = null;
                            $shinsei['patient_birthday_nosub']['month'] = null;
                            $shinsei['patient_birthday_nosub']['day'] = null;
                        }
                        ?>
                        <div>
                            生年月日
                            <?php echo $form->select('ShinseiDetail.patient_birthday_nosub.year', $selectBirthYear, array('empty' => true, 'value' => $shinsei['patient_birthday_nosub']['year'])) ?>
                            年
                            <?php echo $form->select('ShinseiDetail.patient_birthday_nosub.month', $selectMonth, array('empty' => true, 'value' => $shinsei['patient_birthday_nosub']['month'])) ?>
                            月
                            <?php echo $form->select('ShinseiDetail.patient_birthday_nosub.day', $selectDate, array('empty' => true, 'value' => $shinsei['patient_birthday_nosub']['day'])) ?>
                            日 <input type="hidden" class="cal" />
                            </br>
                            (胎児週齢
                            <?php echo $form->hidden('ShinseiDetail.patient_taiji_syurei_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'patient_taiji_syurei_nosub', 'id'))); ?>
                            <?php echo $form->hidden('ShinseiDetail.patient_taiji_syurei_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                            <?php echo $form->hidden('ShinseiDetail.patient_taiji_syurei_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                            <?php echo $form->hidden('ShinseiDetail.patient_taiji_syurei_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                            <?php echo $form->hidden('ShinseiDetail.patient_taiji_syurei_nosub.ssection', array('value' => 'patient_taiji_syurei')); ?>
                            <?php echo $form->hidden('ShinseiDetail.patient_taiji_syurei_nosub.skey', array('value' => 'nosub')); ?>
                            <?php echo $form->input('ShinseiDetail.patient_taiji_syurei_nosub.svalue', array('type' => 'text', 'size' => '5', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'patient_taiji_syurei_nosub', 'svalue'))); ?> 週)
                        </div>

                    </td>
                    <td width="30%">
                        <div>性別：</div>
                        <div>
                            <?php echo $form->hidden('ShinseiDetail.patient_sex_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'patient_sex_nosub', 'id'))); ?>
                            <?php echo $form->hidden('ShinseiDetail.patient_sex_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                            <?php echo $form->hidden('ShinseiDetail.patient_sex_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                            <?php echo $form->hidden('ShinseiDetail.patient_sex_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                            <?php echo $form->hidden('ShinseiDetail.patient_sex_nosub.ssection', array('value' => 'patient_sex')); ?>
                            <?php echo $form->hidden('ShinseiDetail.patient_sex_nosub.skey', array('value' => 'nosub')); ?>
                            <?php
                            echo $form->input('ShinseiDetail.patient_sex_nosub.svalue', array('type' => 'radio', 'options' => array('1' => '男', '2' => '女'),
                                'separator' => '　', 'legend' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'patient_sex_nosub', 'svalue'), 'label' => false))
                            ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        被験者の体質：過敏症素因　
                        <?php echo $form->hidden('ShinseiDetail.patient_taishitsu_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'patient_taishitsu_nosub', 'id'))); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_taishitsu_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_taishitsu_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_taishitsu_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_taishitsu_nosub.ssection', array('value' => 'patient_taishitsu')); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_taishitsu_nosub.skey', array('value' => 'nosub')); ?>
                        <?php
                        echo $form->input('ShinseiDetail.patient_taishitsu_nosub.svalue', array('type' => 'radio', 'options' => array('1' => '無', '9' => '有'),
                            'separator' => '　', 'legend' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'patient_taishitsu_nosub', 'svalue'), 'label' => false))
                        ?>
                        (
                        <?php echo $form->hidden('ShinseiDetail.patient_taishitsu_contents_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'patient_taishitsu_contents_nosub', 'id'))); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_taishitsu_contents_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_taishitsu_contents_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_taishitsu_contents_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_taishitsu_contents_nosub.ssection', array('value' => 'patient_taishitsu_contents')); ?>
                        <?php echo $form->hidden('ShinseiDetail.patient_taishitsu_contents_nosub.skey', array('value' => 'nosub')); ?>
                        <?php echo $form->input('ShinseiDetail.patient_taishitsu_contents_nosub.svalue', array('type' => 'text', 'size' => '25', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'patient_taishitsu_contents_nosub', 'svalue'))); ?>
                        )
                    </td>
                </tr>
            </table>
            <br />

            <?php
            $maxCount = 1;
            $count = 1;
            $arr = $shinsei;
            do {
                $name = 'yugai_jisyo_name_nosub_' . $count;

                if (array_key_exists($name, $arr)) {
                    $count++;
                } else {
                    break;
                }
            } while (1);
            if ($maxCount < ($count - 1)) {
                $maxCount = $count - 1;
            }
            // 行追加しない
            $maxCount = 1;
            ?>
            <?php for ($i = 1; $i <= $maxCount; $i++) { ?>
                <div class="fmt_category_wrapper" id="jutokuContent_<?php echo $i; ?>">

                    <div class="fmt_left fmt_category_title">重篤な有害事象に関する情報</div>
                    <div class="fmt_right">
                        <?php
                        $display = "";
                        if ($i == 0) {
                            $display = "style='display:none;'";
                        }
                        ?>
                    </div>
                    <table class="table_fmt table_list" cellspacing=0 cellpadding=0 border="0" width="100%">
                        <tr>
                            <th width="25%">有害事象名(診断名)</th>
                            <th width="25%">発現日</th>
                            <th width="25%">重篤と判断した理由<br />（複数選択可）</th>
                            <th width="25%">有害事象の転帰<br />転帰日</th>
                        </tr>
                        <tr>
                            <td>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_name_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_name_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_name_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_name_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_name_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_name_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_name_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_name')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_name_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_name_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_name_occur", 'style' => "display:none")) ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_name_nosub_' . $i . '.svalue', array('type' => 'text', 'size' => '29', 'label' => false, 'value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_name_nosub_' . $i, 'svalue'))) ?>

                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_kichi_michi_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_kichi_michi_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_kichi_michi_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_kichi_michi_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_kichi_michi_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_kichi_michi_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_kichi_michi_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_kichi_michi')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_kichi_michi_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_kichi_michi_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_kichi_occur", 'style' => "display:none")) ?>
                                <?php
                                echo $form->input('ShinseiDetail.yugai_jisyo_kichi_michi_nosub_' . $i . '.svalue', array('type' => 'radio', 'options' => array('1' => '既知', '2' => '未知'),
                                    'separator' => '　', 'legend' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_kichi_michi_nosub_' . $i, 'svalue'), 'label' => false))
                                ?>
                            </td>

                            <td>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_hatsugen_date_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_hatsugen_date_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_hatsugen_date_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_hatsugen_date_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_hatsugen_date_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_hatsugen_date_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_hatsugen_date_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_hatsugen_date')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_hatsugen_date_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_hatsugen_date_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_hatsugen_date_occur", 'style' => "display:none")) ?>
                                <?php
                                if (!empty($shinsei['yugai_jisyo_hatsugen_date_nosub_' . $i]['svalue'])) {
                                    if (!is_array($shinsei['yugai_jisyo_hatsugen_date_nosub_' . $i]['svalue'])) {
                                        $date = explode('-', $shinsei['yugai_jisyo_hatsugen_date_nosub_' . $i]['svalue']);
                                        $shinsei['yugai_jisyo_hatsugen_date_nosub_' . $i] = array();
                                        $shinsei['yugai_jisyo_hatsugen_date_nosub_' . $i]['year'] = $date[0];
                                        $shinsei['yugai_jisyo_hatsugen_date_nosub_' . $i]['month'] = $date[1];
                                        $shinsei['yugai_jisyo_hatsugen_date_nosub_' . $i]['day'] = $date[2];
                                    }
                                } else {
                                    $shinsei['yugai_jisyo_hatsugen_date_nosub_' . $i] = array();
                                    $shinsei['yugai_jisyo_hatsugen_date_nosub_' . $i]['year'] = null;
                                    $shinsei['yugai_jisyo_hatsugen_date_nosub_' . $i]['month'] = null;
                                    $shinsei['yugai_jisyo_hatsugen_date_nosub_' . $i]['day'] = null;
                                }
                                ?>
                                <div>
                                    <?php echo $form->select('ShinseiDetail.yugai_jisyo_hatsugen_date_nosub_' . $i . '.year', $selectYear2, array('empty' => true, 'value' => $shinsei['yugai_jisyo_hatsugen_date_nosub_' . $i]['year'])) ?>
                                    年
                                    <?php echo $form->select('ShinseiDetail.yugai_jisyo_hatsugen_date_nosub_' . $i . '.month', $selectMonth, array('empty' => true, 'value' => $shinsei['yugai_jisyo_hatsugen_date_nosub_' . $i]['month'])) ?>
                                    月
                                    <?php echo $form->select('ShinseiDetail.yugai_jisyo_hatsugen_date_nosub_' . $i . '.day', $selectDate, array('empty' => true, 'value' => $shinsei['yugai_jisyo_hatsugen_date_nosub_' . $i]['day'])) ?>
                                    日 <input type="hidden" class="cal" />
                                </div>
                            </td>

                            <td>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_die_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_die_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_die_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_die_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_die_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_die_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_die_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_die')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_die_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_die_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_die_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.yugai_jisyo_die_nosub_' . $i . '.svalue', array('checked' => $shoshikiutil->value($shinsei, 'yugai_jisyo_die_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>死亡

                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_die_osore_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_die_osore_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_die_osore_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_die_osore_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_die_osore_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_die_osore_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_die_osore_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_die_osore')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_die_osore_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_die_osore_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_die_osore_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.yugai_jisyo_die_osore_nosub_' . $i . '.svalue', array('checked' => $shoshikiutil->value($shinsei, 'yugai_jisyo_die_osore_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>死亡の恐れ
                                <br />

                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_nyuin_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_nyuin_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_nyuin_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_nyuin_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_nyuin_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_nyuin_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_nyuin_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_nyuin')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_nyuin_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_nyuin_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_nyuin_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.yugai_jisyo_nyuin_nosub_' . $i . '.svalue', array('checked' => $shoshikiutil->value($shinsei, 'yugai_jisyo_nyuin_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>入院又は入院期間の延長
                                <br />

                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_syogai_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_syogai_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_syogai_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_syogai_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_syogai_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_syogai_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_syogai_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_syogai')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_syogai_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_syogai_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_syogai_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.yugai_jisyo_syogai_nosub_' . $i . '.svalue', array('checked' => $shoshikiutil->value($shinsei, 'yugai_jisyo_syogai_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                障害　

                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_syogai_osore_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_syogai_osore_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_syogai_osore_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_syogai_osore_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_syogai_osore_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_syogai_osore_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_syogai_osore_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_syogai_osore')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_syogai_osore_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_syogai_osore_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_syogai_osore_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.yugai_jisyo_syogai_osore_nosub_' . $i . '.svalue', array('checked' => $shoshikiutil->value($shinsei, 'yugai_jisyo_syogai_osore_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                障害のおそれ　
                                <br />

                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_jyutoku_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_jyutoku_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_jyutoku_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_jyutoku_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_jyutoku_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_jyutoku_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_jyutoku_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_jyutoku')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_jyutoku_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_jyutoku_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_jyutoku_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.yugai_jisyo_jyutoku_nosub_' . $i . '.svalue', array('checked' => $shoshikiutil->value($shinsei, 'yugai_jisyo_jyutoku_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                上記に準じて重篤　

                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_senten_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_senten_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_senten_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_senten_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_senten_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_senten_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_senten_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_senten')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_senten_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_senten_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_senten_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.yugai_jisyo_senten_nosub_' . $i . '.svalue', array('checked' => $shoshikiutil->value($shinsei, 'yugai_jisyo_senten_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                先天異常　

                            </td>
                            <td>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_date_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_date_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_date_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_date_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_date_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_date_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_date_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_tenki_date')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_date_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_tenki_date_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_tenki_date_occur", 'style' => "display:none")) ?>
                                <?php
                                if (!empty($shinsei['yugai_jisyo_tenki_date_nosub_' . $i]['svalue'])) {
                                    if (!is_array($shinsei['yugai_jisyo_tenki_date_nosub_' . $i]['svalue'])) {
                                        $date = explode('-', $shinsei['yugai_jisyo_tenki_date_nosub_' . $i]['svalue']);
                                        $shinsei['yugai_jisyo_tenki_date_nosub_' . $i] = array();
                                        $shinsei['yugai_jisyo_tenki_date_nosub_' . $i]['year'] = $date[0];
                                        $shinsei['yugai_jisyo_tenki_date_nosub_' . $i]['month'] = $date[1];
                                        $shinsei['yugai_jisyo_tenki_date_nosub_' . $i]['day'] = $date[2];
                                    }
                                } else {
                                    $shinsei['yugai_jisyo_tenki_date_nosub_' . $i] = array();
                                    $shinsei['yugai_jisyo_tenki_date_nosub_' . $i]['year'] = null;
                                    $shinsei['yugai_jisyo_tenki_date_nosub_' . $i]['month'] = null;
                                    $shinsei['yugai_jisyo_tenki_date_nosub_' . $i]['day'] = null;
                                }
                                ?>
                                <div>
                                    <?php echo $form->select('ShinseiDetail.yugai_jisyo_tenki_date_nosub_' . $i . '.year', $selectYear2, array('empty' => true, 'value' => $shinsei['yugai_jisyo_tenki_date_nosub_' . $i]['year'])) ?>
                                    年
                                    <?php echo $form->select('ShinseiDetail.yugai_jisyo_tenki_date_nosub_' . $i . '.month', $selectMonth, array('empty' => true, 'value' => $shinsei['yugai_jisyo_tenki_date_nosub_' . $i]['month'])) ?>
                                    月
                                    <?php echo $form->select('ShinseiDetail.yugai_jisyo_tenki_date_nosub_' . $i . '.day', $selectDate, array('empty' => true, 'value' => $shinsei['yugai_jisyo_tenki_date_nosub_' . $i]['day'])) ?>
                                    日 <input type="hidden" class="cal" />
                                </div>

                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_recover_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_recover_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_recover_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_recover_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_recover_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_recover_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_recover_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_tenki_recover')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_recover_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_tenki_recover_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_tenki_recover_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.yugai_jisyo_tenki_recover_nosub_' . $i . '.svalue', array('class' => 'yugai_jisyo_tenki' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_recover_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                回復　

                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_keikai_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_keikai_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_keikai_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_keikai_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_keikai_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_keikai_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_keikai_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_tenki_keikai')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_keikai_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_tenki_keikai_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_tenki_keikai_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.yugai_jisyo_tenki_keikai_nosub_' . $i . '.svalue', array('class' => 'yugai_jisyo_tenki' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_keikai_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                軽快　

                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_mikaihuku_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_mikaihuku_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_mikaihuku_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_mikaihuku_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_mikaihuku_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_mikaihuku_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_mikaihuku_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_tenki_mikaihuku')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_mikaihuku_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_tenki_mikaihuku_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_tenki_mikaihuku_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.yugai_jisyo_tenki_mikaihuku_nosub_' . $i . '.svalue', array('class' => 'yugai_jisyo_tenki' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_mikaihuku_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                未回復
                                <br />

                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_koisyo_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_koisyo_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_koisyo_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_koisyo_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_koisyo_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_koisyo_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_koisyo_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_tenki_koisyo')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_koisyo_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_tenki_koisyo_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_tenki_koisyo_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.yugai_jisyo_tenki_koisyo_nosub_' . $i . '.svalue', array('class' => 'yugai_jisyo_tenki' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_koisyo_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                後遺症あり

                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_die_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_die_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_die_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_die_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_die_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_die_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_die_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_tenki_die')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_die_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_tenki_die_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_tenki_die_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.yugai_jisyo_tenki_die_nosub_' . $i . '.svalue', array('class' => 'yugai_jisyo_tenki' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_die_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                死亡　

                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_unknown_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_unknown_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_unknown_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_unknown_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_unknown_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_unknown_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_unknown_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_tenki_unknown')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_unknown_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_tenki_unknown_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_tenki_unknown_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.yugai_jisyo_tenki_unknown_nosub_' . $i . '.svalue', array('class' => 'yugai_jisyo_tenki' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_unknown_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                不明
                                <br />

                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_gaitosezu_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_gaitosezu_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_gaitosezu_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_gaitosezu_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_gaitosezu_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_gaitosezu_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_gaitosezu_nosub_' . $i . '.ssection', array('value' => 'yugai_jisyo_tenki_gaitosezu')); ?>
                                <?php echo $form->hidden('ShinseiDetail.yugai_jisyo_tenki_gaitosezu_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.yugai_jisyo_tenki_gaitosezu_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_tenki_unknown_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.yugai_jisyo_tenki_gaitosezu_nosub_' . $i . '.svalue', array('class' => 'yugai_jisyo_tenki' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_gaitosezu_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                該当せず
                            </td>
                        </tr>
                    </table>
                    <div>
                        <p style="font-size:smaller;">＊：研究計画書の記載に基づいて判断する。記載内容と性質や重症度が一致する場合は「既知」に該当する。</p>
                        <p style="padding-left:2em;font-size:smaller;">記載されていてもその性質や重症度が記載内容と一致しない場合（急性腎不全に対する“間質性腎炎”、肝炎に対する“劇症肝炎”等）は「未知」に該当する。</p>
                    </div>
                    <div class="fmt_category_title">医療機器に関する情報等</div>
                    <table class="table_fmt table_list" cellspacing=0 cellpadding=0 border="0" width="100%">
                        <tr>
                            <th width="25%" colspan="2">
                                適応期間
                            </th>
                            <td>
                                <?php echo $form->hidden('ShinseiDetail.drug_toyo_start_date_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_toyo_start_date_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_toyo_start_date_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_toyo_start_date_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_toyo_start_date_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_toyo_start_date_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_toyo_start_date_nosub_' . $i . '.ssection', array('value' => 'drug_toyo_start_date')); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_toyo_start_date_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.drug_toyo_start_date_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_toyo_start_date_occur", 'style' => "display:none")) ?>
                                <?php
                                    if (!empty($shinsei['drug_toyo_start_date_nosub_' . $i]['svalue'])) {
                                        if (!is_array($shinsei['drug_toyo_start_date_nosub_' . $i]['svalue'])) {
                                            $date = explode('-', $shinsei['drug_toyo_start_date_nosub_' . $i]['svalue']);
                                            $shinsei['drug_toyo_start_date_nosub_' . $i] = array();
                                            $shinsei['drug_toyo_start_date_nosub_' . $i]['year'] = $date[0];
                                            $shinsei['drug_toyo_start_date_nosub_' . $i]['month'] = $date[1];
                                            $shinsei['drug_toyo_start_date_nosub_' . $i]['day'] = $date[2];
                                        }
                                    } else {
                                        $shinsei['drug_toyo_start_date_nosub_' . $i] = array();
                                        $shinsei['drug_toyo_start_date_nosub_' . $i]['year'] = null;
                                        $shinsei['drug_toyo_start_date_nosub_' . $i]['month'] = null;
                                        $shinsei['drug_toyo_start_date_nosub_' . $i]['day'] = null;
                                    }
                                ?>
                                <div style="float:left;">
                                    <?php echo $form->select('ShinseiDetail.drug_toyo_start_date_nosub_' . $i . '.year', $selectYear2, array('empty' => true, 'value' => $shinsei['drug_toyo_start_date_nosub_' . $i]['year'])) ?>
                                    年
                                    <?php echo $form->select('ShinseiDetail.drug_toyo_start_date_nosub_' . $i . '.month', $selectMonth, array('empty' => true, 'value' => $shinsei['drug_toyo_start_date_nosub_' . $i]['month'])) ?>
                                    月
                                    <?php echo $form->select('ShinseiDetail.drug_toyo_start_date_nosub_' . $i . '.day', $selectDate, array('empty' => true, 'value' => $shinsei['drug_toyo_start_date_nosub_' . $i]['day'])) ?>
                                    日 <input type="hidden" class="cal" />
                                </div>
                                <div style="float:left;">
              　～
                                </div>
                                <div style="float:left;">
                                    <div>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_is_end_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_toyo_is_end_nosub_' . $i, 'id'))); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_is_end_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_is_end_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_is_end_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_is_end_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_is_end_nosub_' . $i . '.ssection', array('value' => 'drug_toyo_is_end')); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_is_end_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                        <?php echo $form->input('ShinseiDetail.drug_toyo_is_end_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_toyo_is_end_occur", 'style' => "display:none")) ?>
                                        <?php echo $form->checkbox('ShinseiDetail.drug_toyo_is_end_nosub_' . $i . '.svalue', array('class' => 'drug_toyo_end' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'drug_toyo_is_end_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>

                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_end_date_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_toyo_end_date_nosub_' . $i, 'id'))); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_end_date_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_end_date_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_end_date_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_end_date_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_end_date_nosub_' . $i . '.ssection', array('value' => 'drug_toyo_end_date')); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_end_date_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                        <?php echo $form->input('ShinseiDetail.drug_toyo_end_date_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_toyo_end_date_occur", 'style' => "display:none")) ?>

                                        <?php
                                        if (!empty($shinsei['drug_toyo_end_date_nosub_' . $i]['svalue'])) {
                                            if (!is_array($shinsei['drug_toyo_end_date_nosub_' . $i]['svalue'])) {
                                                $date = explode('-', $shinsei['drug_toyo_end_date_nosub_' . $i]['svalue']);
                                                $shinsei['drug_toyo_end_date_nosub_' . $i] = array();
                                                $shinsei['drug_toyo_end_date_nosub_' . $i]['year'] = $date[0];
                                                $shinsei['drug_toyo_end_date_nosub_' . $i]['month'] = $date[1];
                                                $shinsei['drug_toyo_end_date_nosub_' . $i]['day'] = $date[2];
                                            }
                                        } else {
                                            $shinsei['drug_toyo_end_date_nosub_' . $i] = array();
                                            $shinsei['drug_toyo_end_date_nosub_' . $i]['year'] = null;
                                            $shinsei['drug_toyo_end_date_nosub_' . $i]['month'] = null;
                                            $shinsei['drug_toyo_end_date_nosub_' . $i]['day'] = null;
                                        }
                                        ?>

                                        <?php echo $form->select('ShinseiDetail.drug_toyo_end_date_nosub_' . $i . '.year', $selectYear2, array('empty' => true, 'value' => $shinsei['drug_toyo_end_date_nosub_' . $i]['year'])) ?>
                                        年
                                        <?php echo $form->select('ShinseiDetail.drug_toyo_end_date_nosub_' . $i . '.month', $selectMonth, array('empty' => true, 'value' => $shinsei['drug_toyo_end_date_nosub_' . $i]['month'])) ?>
                                        月
                                        <?php echo $form->select('ShinseiDetail.drug_toyo_end_date_nosub_' . $i . '.day', $selectDate, array('empty' => true, 'value' => $shinsei['drug_toyo_end_date_nosub_' . $i]['day'])) ?>
                                        日 <input type="hidden" class="cal" />
                                    </div>
                                    <div>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_is_toyo_chu_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_toyo_is_toyo_chu_nosub_' . $i, 'id'))); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_is_toyo_chu_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_is_toyo_chu_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_is_toyo_chu_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_is_toyo_chu_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_is_toyo_chu_nosub_' . $i . '.ssection', array('value' => 'drug_toyo_is_toyo_chu')); ?>
                                        <?php echo $form->hidden('ShinseiDetail.drug_toyo_is_toyo_chu_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                        <?php echo $form->input('ShinseiDetail.drug_toyo_is_toyo_chu_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_toyo_is_toyo_chu_occur", 'style' => "display:none")) ?>
                                        <?php echo $form->checkbox('ShinseiDetail.drug_toyo_is_toyo_chu_nosub_' . $i . '.svalue', array('class' => 'drug_toyo_end' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'drug_toyo_is_toyo_chu_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?> 適応中
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                有害事象発現後の措置
                            </th>
                            <td>
                                <?php echo $form->hidden(sprintf('ShinseiDetail.drug_sochi_nosub_%s.id', $i), array('value' => $shoshikiutil->value($shinsei, sprintf('drug_sochi_nosub_%s', $i), 'id'))); ?>
                                <?php echo $form->hidden(sprintf('ShinseiDetail.drug_sochi_nosub_%s.deleted', $i), array('value' => 0)); ?>
                                <?php echo $form->hidden(sprintf('ShinseiDetail.drug_sochi_nosub_%s.project_shinsei_id', $i), array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden(sprintf('ShinseiDetail.drug_sochi_nosub_%s.shinsei_id', $i), array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden(sprintf('ShinseiDetail.drug_sochi_nosub_%s.shoshiki_name', $i), array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden(sprintf('ShinseiDetail.drug_sochi_nosub_%s.ssection', $i), array('value' => 'drug_sochi')); ?>
                                <?php echo $form->hidden(sprintf('ShinseiDetail.drug_sochi_nosub_%s.skey', $i), array('value' => 'nosub')); ?>
                                <?php echo $form->input(sprintf('ShinseiDetail.drug_sochi_nosub_%s.occurs', $i), array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_sochir", 'style' => "display:none")) ?>
                                <?php
                                echo $form->input(sprintf('ShinseiDetail.drug_sochi_nosub_%s.svalue', $i), array('type' => 'radio', 'options' => array('1' => '無', '9' => '有'),
                                    'separator' => '　', 'legend' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, sprintf('drug_sochi_nosub_%s', $i), 'svalue'), 'label' => false))
                                ?>

                                <?php echo $form->hidden('ShinseiDetail.drug_sochi_remarks_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_sochi_remarks_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_sochi_remarks_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_sochi_remarks_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_sochi_remarks_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_sochi_remarks_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_sochi_remarks_nosub_' . $i . '.ssection', array('value' => 'drug_sochi_remarks')); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_sochi_remarks_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.drug_sochi_remarks_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_sochi_remarks", 'style' => "display:none")) ?>
                                （<?php echo $form->input('ShinseiDetail.drug_sochi_remarks_nosub_' . $i . '.svalue', array('type' => 'text', 'size' => '29', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'drug_sochi_remarks_nosub_' . $i, 'svalue'))) ?>）
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                医療機器の<br />不具合状態
                            </th>
                            <td>
                                <div style="font-size:smaller;">
                                    （不具合のあった医療機器等と、その状態（構造的・材質的・機能的欠陥について具体的に記載）
                                </div>
                                <div>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.drug_fuguai_jyotai_nosub_%s.id', $i), array('value' => $shoshikiutil->value($shinsei, sprintf('drug_fuguai_jyotai_nosub_%s', $i), 'id'))); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.drug_fuguai_jyotai_nosub_%s.deleted', $i), array('value' => 0)); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.drug_fuguai_jyotai_nosub_%s.project_shinsei_id', $i), array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.drug_fuguai_jyotai_nosub_%s.shinsei_id', $i), array('value' => $shinsei_id)); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.drug_fuguai_jyotai_nosub_%s.shoshiki_name', $i), array('value' => $shoshiki_name)); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.drug_fuguai_jyotai_nosub_%s.ssection', $i), array('value' => 'drug_fuguai_jyotai')); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.drug_fuguai_jyotai_nosub_%s.skey', $i), array('value' => 'nosub')); ?>
                                    <?php echo $form->input(sprintf('ShinseiDetail.drug_fuguai_jyotai_nosub_%s.occurs', $i), array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_fuguai_jyotai_occur", 'style' => "display:none")) ?>
                                    <?php echo $form->input(sprintf('ShinseiDetail.drug_fuguai_jyotai_nosub_%s.svalue', $i), array('type' => 'textarea', 'rows' => '2', 'cols' => '95', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, sprintf('drug_fuguai_jyotai_nosub_%s', $i), 'svalue'))); ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2">
                                不具合が発生した医療機器
                            </th>
                            <td>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_kenkyutaisyo_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_hassei_kiki_kenkyutaisyo_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_kenkyutaisyo_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_kenkyutaisyo_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_kenkyutaisyo_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_kenkyutaisyo_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_kenkyutaisyo_nosub_' . $i . '.ssection', array('value' => 'drug_hassei_kiki_kenkyutaisyo')); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_kenkyutaisyo_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.drug_hassei_kiki_kenkyutaisyo_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_hassei_kiki_kenkyutaisyo_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.drug_hassei_kiki_kenkyutaisyo_nosub_' . $i . '.svalue', array('class' => 'drug_hassei_kiki' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'drug_hassei_kiki_kenkyutaisyo_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                研究対象の機器

                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_other_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_hassei_kiki_other_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_other_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_other_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_other_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_other_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_other_nosub_' . $i . '.ssection', array('value' => 'drug_hassei_kiki_other')); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_other_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.drug_hassei_kiki_other_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_hassei_kiki_other_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.drug_hassei_kiki_other_nosub_' . $i . '.svalue', array('class' => 'drug_hassei_kiki' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'drug_hassei_kiki_other_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                その他

                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_remarks_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_hassei_kiki_remarks_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_remarks_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_remarks_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_remarks_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_remarks_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_remarks_nosub_' . $i . '.ssection', array('value' => 'drug_hassei_kiki_remarks')); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_hassei_kiki_remarks_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.drug_hassei_kiki_remarks_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_name_occur", 'style' => "display:none")) ?>
                                （<?php echo $form->input('ShinseiDetail.drug_hassei_kiki_remarks_nosub_' . $i . '.svalue', array('type' => 'text', 'size' => '29', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'drug_hassei_kiki_remarks_nosub_' . $i, 'svalue'))) ?>）
                            </td>
                        </tr>
                        <tr>
                            <th rowspan="2">
                                因果<br />関係
                            </th>
                            <th>
                                有害事象と医療機器
                            </th>
                            <td>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_hitei_dekinai_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_inga_hitei_dekinai_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_hitei_dekinai_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_hitei_dekinai_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_hitei_dekinai_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_hitei_dekinai_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_hitei_dekinai_nosub_' . $i . '.ssection', array('value' => 'drug_inga_hitei_dekinai')); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_hitei_dekinai_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.drug_inga_hitei_dekinai_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_inga_hitei_dekinai_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.drug_inga_hitei_dekinai_nosub_' . $i . '.svalue', array('class' => 'drug_inga' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'drug_inga_hitei_dekinai_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                否定できない

                                <?php echo $form->hidden('ShinseiDetail.drug_inga_hitei_dekiru_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_inga_hitei_dekiru_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_hitei_dekiru_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_hitei_dekiru_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_hitei_dekiru_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_hitei_dekiru_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_hitei_dekiru_nosub_' . $i . '.ssection', array('value' => 'drug_inga_hitei_dekiru')); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_hitei_dekiru_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.drug_inga_hitei_dekiru_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_inga_hitei_dekiru_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.drug_inga_hitei_dekiru_nosub_' . $i . '.svalue', array('class' => 'drug_inga' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'drug_inga_hitei_dekiru_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                否定できる

                                <?php echo $form->hidden('ShinseiDetail.drug_inga_unknown_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_inga_unknown_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_unknown_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_unknown_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_unknown_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_unknown_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_unknown_nosub_' . $i . '.ssection', array('value' => 'drug_inga_unknown')); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_unknown_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.drug_inga_unknown_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_inga_unknown_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.drug_inga_unknown_nosub_' . $i . '.svalue', array('class' => 'drug_inga' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'drug_inga_unknown_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                不明

                                <?php echo $form->hidden('ShinseiDetail.drug_inga_gaitosezu_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_inga_gaitosezu_nosub_' . $i, 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_gaitosezu_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_gaitosezu_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_gaitosezu_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_gaitosezu_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_gaitosezu_nosub_' . $i . '.ssection', array('value' => 'drug_inga_gaitosezu')); ?>
                                <?php echo $form->hidden('ShinseiDetail.drug_inga_gaitosezu_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.drug_inga_gaitosezu_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_inga_gaitosezu_occur", 'style' => "display:none")) ?>
                                <?php echo $form->checkbox('ShinseiDetail.drug_inga_gaitosezu_nosub_' . $i . '.svalue', array('class' => 'drug_inga' . $i, 'onclick' => 'checkOnlyOneBox(this)', 'checked' => $shoshikiutil->value($shinsei, 'drug_inga_gaitosezu_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                該当せず
                            </td>
                        </tr>
                        <tr>
                            <th>
                                有害事象等と<br />その他の事項
                            </th>
                            <td>
                                <div>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_tewaza_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_inga_tewaza_nosub_' . $i, 'id'))); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_tewaza_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_tewaza_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_tewaza_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_tewaza_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_tewaza_nosub_' . $i . '.ssection', array('value' => 'drug_inga_tewaza')); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_tewaza_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                    <?php echo $form->input('ShinseiDetail.drug_inga_tewaza_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_inga_tewaza_occur", 'style' => "display:none")) ?>
                                    <?php echo $form->checkbox('ShinseiDetail.drug_inga_tewaza_nosub_' . $i . '.svalue', array('class' => 'drug_inga2' . $i, 'checked' => $shoshikiutil->value($shinsei, 'drug_inga_tewaza_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                    手技

                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_tewaza_remarks_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_inga_tewaza_remarks_nosub_' . $i, 'id'))); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_tewaza_remarks_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_tewaza_remarks_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_tewaza_remarks_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_tewaza_remarks_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_tewaza_remarks_nosub_' . $i . '.ssection', array('value' => 'drug_inga_tewaza_remarks')); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_tewaza_remarks_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                    <?php echo $form->input('ShinseiDetail.drug_inga_tewaza_remarks_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_name_occur", 'style' => "display:none")) ?>
                                    （<?php echo $form->input('ShinseiDetail.drug_inga_tewaza_remarks_nosub_' . $i . '.svalue', array('type' => 'text', 'size' => '29', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'drug_inga_tewaza_remarks_nosub_' . $i, 'svalue'))) ?>）
                                </div>
                                <div>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_genshikkan_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_inga_genshikkan_nosub_' . $i, 'id'))); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_genshikkan_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_genshikkan_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_genshikkan_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_genshikkan_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_genshikkan_nosub_' . $i . '.ssection', array('value' => 'drug_inga_genshikkan')); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_genshikkan_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                    <?php echo $form->input('ShinseiDetail.drug_inga_genshikkan_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_inga_genshikkan_occur", 'style' => "display:none")) ?>
                                    <?php echo $form->checkbox('ShinseiDetail.drug_inga_genshikkan_nosub_' . $i . '.svalue', array('class' => 'drug_inga2' . $i, 'checked' => $shoshikiutil->value($shinsei, 'drug_inga_genshikkan_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                    原疾患

                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_genshikkan_remarks_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_inga_genshikkan_remarks_nosub_' . $i, 'id'))); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_genshikkan_remarks_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_genshikkan_remarks_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_genshikkan_remarks_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_genshikkan_remarks_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_genshikkan_remarks_nosub_' . $i . '.ssection', array('value' => 'drug_inga_genshikkan_remarks')); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_genshikkan_remarks_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                    <?php echo $form->input('ShinseiDetail.drug_inga_genshikkan_remarks_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_name_occur", 'style' => "display:none")) ?>
                                    （<?php echo $form->input('ShinseiDetail.drug_inga_genshikkan_remarks_nosub_' . $i . '.svalue', array('type' => 'text', 'size' => '29', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'drug_inga_genshikkan_remarks_nosub_' . $i, 'svalue'))) ?>）
                                </div>
                                <div>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_heiyoyaku_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_inga_heiyoyaku_nosub_' . $i, 'id'))); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_heiyoyaku_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_heiyoyaku_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_heiyoyaku_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_heiyoyaku_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_heiyoyaku_nosub_' . $i . '.ssection', array('value' => 'drug_inga_heiyoyaku')); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_heiyoyaku_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                    <?php echo $form->input('ShinseiDetail.drug_inga_heiyoyaku_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_inga_heiyoyaku_occur", 'style' => "display:none")) ?>
                                    <?php echo $form->checkbox('ShinseiDetail.drug_inga_heiyoyaku_nosub_' . $i . '.svalue', array('class' => 'drug_inga2' . $i, 'checked' => $shoshikiutil->value($shinsei, 'drug_inga_heiyoyaku_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                    併用薬、併用療法

                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_heiyoyaku_remarks_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_inga_heiyoyaku_remarks_nosub_' . $i, 'id'))); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_heiyoyaku_remarks_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_heiyoyaku_remarks_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_heiyoyaku_remarks_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_heiyoyaku_remarks_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_heiyoyaku_remarks_nosub_' . $i . '.ssection', array('value' => 'drug_inga_heiyoyaku_remarks')); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_heiyoyaku_remarks_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                    <?php echo $form->input('ShinseiDetail.drug_inga_heiyoyaku_remarks_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_name_occur", 'style' => "display:none")) ?>
                                    （<?php echo $form->input('ShinseiDetail.drug_inga_heiyoyaku_remarks_nosub_' . $i . '.svalue', array('type' => 'text', 'size' => '29', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'drug_inga_heiyoyaku_remarks_nosub_' . $i, 'svalue'))) ?>）
                                </div>
                                <div>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_other_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_inga_other_nosub_' . $i, 'id'))); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_other_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_other_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_other_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_other_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_other_nosub_' . $i . '.ssection', array('value' => 'drug_inga_other')); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_other_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                    <?php echo $form->input('ShinseiDetail.drug_inga_other_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "drug_inga_other_occur", 'style' => "display:none")) ?>
                                    <?php echo $form->checkbox('ShinseiDetail.drug_inga_other_nosub_' . $i . '.svalue', array('class' => 'drug_inga2' . $i, 'checked' => $shoshikiutil->value($shinsei, 'drug_inga_other_nosub_' . $i, 'svalue'), 'label' => false, 'div' => false)) ?>
                                    その他

                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_other_remarks_nosub_' . $i . '.id', array('value' => $shoshikiutil->value($shinsei, 'drug_inga_other_remarks_nosub_' . $i, 'id'))); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_other_remarks_nosub_' . $i . '.deleted', array('value' => 0)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_other_remarks_nosub_' . $i . '.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_other_remarks_nosub_' . $i . '.shinsei_id', array('value' => $shinsei_id)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_other_remarks_nosub_' . $i . '.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_other_remarks_nosub_' . $i . '.ssection', array('value' => 'drug_inga_other_remarks')); ?>
                                    <?php echo $form->hidden('ShinseiDetail.drug_inga_other_remarks_nosub_' . $i . '.skey', array('value' => 'nosub')); ?>
                                    <?php echo $form->input('ShinseiDetail.drug_inga_other_remarks_nosub_' . $i . '.occurs', array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => "jisyo_name_occur", 'style' => "display:none")) ?>
                                    （<?php echo $form->input('ShinseiDetail.drug_inga_other_remarks_nosub_' . $i . '.svalue', array('type' => 'text', 'size' => '29', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'drug_inga_other_remarks_nosub_' . $i, 'svalue'))) ?>）
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br />

                    <div class="fmt_category_title">
                        経過：
                        <span style="font-size:smaller;font-weight:normal;">
                            重篤な有害事象等発現までの詳細な時間経過、重篤な有害事象等に対する処置、転帰及び関連情報を含む症例の概要を記載する。
                        </span>
                    </div>
                    <table class="table_fmt table_list" cellspacing=0 cellpadding=0 border="0" width="100%">
                        <tr>
                            <th width="30%">
                                西暦年/月/日
                            </th>
                            <th width="70%">
                                内       容
                            </th>
                        </tr>
                            <?php for ($ki = 0; $ki < 11; $ki++): ?>
                            <tr>
                                <td style="text-align:center;">
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.keika_date_%s_nosub_%s.id', $ki, $i), array('value' => $shoshikiutil->value($shinsei, sprintf('keika_date_%s_nosub_%s', $ki, $i), 'id'))); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.keika_date_%s_nosub_%s.deleted', $ki, $i), array('value' => 0)); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.keika_date_%s_nosub_%s.project_shinsei_id', $ki, $i), array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.keika_date_%s_nosub_%s.shinsei_id', $ki, $i), array('value' => $shinsei_id)); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.keika_date_%s_nosub_%s.shoshiki_name', $ki, $i), array('value' => $shoshiki_name)); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.keika_date_%s_nosub_%s.ssection', $ki, $i), array('value' => sprintf('keika_date_%s', $ki))); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.keika_date_%s_nosub_%s.skey', $ki, $i), array('value' => 'nosub')); ?>
                                    <?php echo $form->input(sprintf('ShinseiDetail.keika_date_%s_nosub_%s.occurs', $ki, $i), array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => sprintf("keika_date_%s_occur", $ki), 'style' => "display:none")) ?>
                                    <?php
                                    if (!empty($shinsei[sprintf('keika_date_%s_nosub_%s', $ki, $i)]['svalue'])) {
                                        if (!is_array($shinsei[sprintf('keika_date_%s_nosub_%s', $ki, $i)]['svalue'])) {
                                            $date = explode('-', $shinsei[sprintf('keika_date_%s_nosub_%s', $ki, $i)]['svalue']);
                                            $shinsei[sprintf('keika_date_%s_nosub_%s', $ki, $i)] = array();
                                            $shinsei[sprintf('keika_date_%s_nosub_%s', $ki, $i)]['year'] = $date[0];
                                            $shinsei[sprintf('keika_date_%s_nosub_%s', $ki, $i)]['month'] = $date[1];
                                            $shinsei[sprintf('keika_date_%s_nosub_%s', $ki, $i)]['day'] = $date[2];
                                        }
                                    } else {
                                        $shinsei[sprintf('keika_date_%s_nosub_%s', $ki, $i)] = array();
                                        $shinsei[sprintf('keika_date_%s_nosub_%s', $ki, $i)]['year'] = null;
                                        $shinsei[sprintf('keika_date_%s_nosub_%s', $ki, $i)]['month'] = null;
                                        $shinsei[sprintf('keika_date_%s_nosub_%s', $ki, $i)]['day'] = null;
                                    }
                                    ?>

                                    <div style="float:left;">
                                        <?php echo $form->select(sprintf('ShinseiDetail.keika_date_%s_nosub_%s.year', $ki, $i), $selectYear2, array('empty' => true, 'value' => $shinsei[sprintf('keika_date_%s_nosub_%s', $ki, $i)]['year'])) ?>
                                        年
                                        <?php echo $form->select(sprintf('ShinseiDetail.keika_date_%s_nosub_%s.month', $ki, $i), $selectMonth, array('empty' => true, 'value' => $shinsei[sprintf('keika_date_%s_nosub_%s', $ki, $i)]['month'])) ?>
                                        月
                                    <?php echo $form->select(sprintf('ShinseiDetail.keika_date_%s_nosub_%s.day', $ki, $i), $selectDate, array('empty' => true, 'value' => $shinsei[sprintf('keika_date_%s_nosub_%s', $ki, $i)]['day'])) ?>
                                        日 <input type="hidden" class="cal" />
                                    </div>

                                </td>
                                <td>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.keika_contents_%s_nosub_%s.id', $ki, $i), array('value' => $shoshikiutil->value($shinsei, sprintf('keika_contents_%s_nosub_%s', $ki, $i), 'id'))); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.keika_contents_%s_nosub_%s.deleted', $ki, $i), array('value' => 0)); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.keika_contents_%s_nosub_%s.project_shinsei_id', $ki, $i), array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.keika_contents_%s_nosub_%s.shinsei_id', $ki, $i), array('value' => $shinsei_id)); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.keika_contents_%s_nosub_%s.shoshiki_name', $ki, $i), array('value' => $shoshiki_name)); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.keika_contents_%s_nosub_%s.ssection', $ki, $i), array('value' => sprintf('keika_contents_%s', $ki))); ?>
                                    <?php echo $form->hidden(sprintf('ShinseiDetail.keika_contents_%s_nosub_%s.skey', $ki, $i), array('value' => 'nosub')); ?>
                                    <?php echo $form->input(sprintf('ShinseiDetail.keika_contents_%s_nosub_%s.occurs', $ki, $i), array('type' => 'text', 'size' => '3', 'label' => false, 'div' => false, 'value' => $i, "class" => sprintf("keika_contents_%s_occur", $ki), 'style' => "display:none")) ?>
                                    <?php echo $form->input(sprintf('ShinseiDetail.keika_contents_%s_nosub_%s.svalue', $ki, $i), array('type' => 'textarea', 'rows' => '1', 'cols' => '90', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, sprintf('keika_contents_%s_nosub_%s', $ki, $i), 'svalue'))); ?>
                                </td>
                            </tr>
                        <?php endfor; ?>
                    </table>
                    <br />
                    <div class="fmt_category_title">
                        コメント：
                        <span style="font-size:smaller;font-weight:normal;">
                            医療機器との因果関係の判断根拠、並びに、重篤な有害事象の診断、重篤性、併用薬・療法（医療機器を含む）との相互作用等について記載する。
                        </span>
                    </div>
                    <table class="table_fmt table_list" cellspacing=0 cellpadding=0 border="0" width="100%">
                        <tr>
                            <td>
                                <?php echo $form->hidden('ShinseiDetail.comment_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'comment_nosub', 'id'))); ?>
                                <?php echo $form->hidden('ShinseiDetail.comment_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                                <?php echo $form->hidden('ShinseiDetail.comment_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                                <?php echo $form->hidden('ShinseiDetail.comment_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                                <?php echo $form->hidden('ShinseiDetail.comment_nosub.ssection', array('value' => 'comment')); ?>
                                <?php echo $form->hidden('ShinseiDetail.comment_nosub.skey', array('value' => 'nosub')); ?>
                                <?php echo $form->input('ShinseiDetail.comment_nosub.svalue', array('type' => 'textarea', 'rows' => '2', 'cols' => '120', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'comment_nosub', 'svalue'))); ?>
                            </td>
                        </tr>
                    </table>

                </div><!-- id=category_wrapper -->
            <?php } ?>
            <table class="table_fmt" cellspacing=0 cellpadding=0 border="0" width="100%">
                <tr>
                    <th class="fmt_label_width">
                        添付資料
                    </th>
                    <td>
                        <?php echo $form->hidden('ShinseiDetail.tenpu_shiryo_name_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'tenpu_shiryo_name_nosub', 'id'))); ?>
                        <?php echo $form->hidden('ShinseiDetail.tenpu_shiryo_name_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                        <?php echo $form->hidden('ShinseiDetail.tenpu_shiryo_name_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                        <?php echo $form->hidden('ShinseiDetail.tenpu_shiryo_name_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                        <?php echo $form->hidden('ShinseiDetail.tenpu_shiryo_name_nosub.ssection', array('value' => 'tenpu_shiryo_name')); ?>
                        <?php echo $form->hidden('ShinseiDetail.tenpu_shiryo_name_nosub.skey', array('value' => 'nosub')); ?>
                        <?php echo $form->input('ShinseiDetail.tenpu_shiryo_name_nosub.svalue', array('type' => 'textarea', 'rows' => '2', 'cols' => '105', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'tenpu_shiryo_name_nosub', 'svalue'))); ?>
                    </td>
                </tr>
            </table>
            <br/>

            <table class="table_fmt" cellspacing=0 cellpadding=0 border="0" width="100%">
                <tr>
                    <th class="fmt_label_width">
                        備考
                    </th>
                    <td>
                        <?php echo $form->hidden('ShinseiDetail.remarks_nosub.id', array('value' => $shoshikiutil->value($shinsei, 'remarks_nosub', 'id'))); ?>
                        <?php echo $form->hidden('ShinseiDetail.remarks_nosub.project_shinsei_id', array('value' => isset($project_shinsei_id) ? $project_shinsei_id : null)); ?>
                        <?php echo $form->hidden('ShinseiDetail.remarks_nosub.shinsei_id', array('value' => $shinsei_id)); ?>
                        <?php echo $form->hidden('ShinseiDetail.remarks_nosub.shoshiki_name', array('value' => $shoshiki_name)); ?>
                        <?php echo $form->hidden('ShinseiDetail.remarks_nosub.ssection', array('value' => 'remarks')); ?>
                        <?php echo $form->hidden('ShinseiDetail.remarks_nosub.skey', array('value' => 'nosub')); ?>
                        <?php echo $form->input('ShinseiDetail.remarks_nosub.svalue', array('type' => 'textarea', 'rows' => '2', 'cols' => '105', 'label' => false, 'div' => false, 'value' => $shoshikiutil->value($shinsei, 'remarks_nosub', 'svalue'))); ?>
                    </td>
                </tr>
            </table>
            <br/>

        </div><!-- id=fmt_contents -->

        <div id="fmt_footer">
        </div><!-- id=fmt_footer -->

    </div><!-- id=fmt_wrapper -->

    <div class="fmt_clear" style="padding-bottom:15px;"></div>

