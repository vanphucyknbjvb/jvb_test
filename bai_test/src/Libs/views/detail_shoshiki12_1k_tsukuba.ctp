<link rel="stylesheet" type="text/css" href="/css/style_formatOutput.css" />

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table_03" id="infoArea">
<td width="15" colspan="2" style="background-color:#CCFF99;"><strong>重篤な有害事象及び不具合に関する報告書</strong></td>
</table>

<div id="fmt_wrapper">
<div id="fmt_header">
  <div id="fmt_header_left">
  （研究実施責任者→研究機関の長）医療機器用
  </div>
  
  <div id="fmt_header_right">
    <!-- 申請日 -->
    <?php echo $date->datecb($shoshikiutil->value($shinsei,'shinsei_date_nosub', 'svalue')) ?>
  </div>
</div>

<div id="fmt_title">
  重篤な有害事象及び不具合に関する報告書（第
  <?php echo $shoshikiutil->value($shinsei,'common_hou_num_nosub', 'svalue') ?>
  報）
</div>

<div class="fmt_contents">

  <div class="fmt_left fmt_fill">
    <span class="fmt_label_uline">研究機関の長</span><br />
    <span class="fmt_label_right">
      <?php echo $shoshikiutil->value($shinsei, 'hospital_nosub', 'svalue') ?>
    </span><br />
    <?php echo $shoshikiutil->value($shinsei, 'hospital_chief_job_nosub', 'svalue') ?>
  </div>

  <div class="fmt_right">
    <span class="fmt_label_uline">研究実施責任者</span><br />
    （氏名）
    <?php echo $shoshikiutil->value($shinsei, 'doctor_name_nosub', 'svalue') ?>
  </div>
  
  <div class="fmt_left fmt_caption fmt_clear">
    下記の臨床研究等において、以下のとおり重篤と判断される有害事象及び不具合等を認めたので報告いたします。
  </div>
  
  <div class="fmt_label_center fmt_caption fmt_clear">
    記
  </div>
  
  <table class="table_fmt" cellspacing=0 cellpadding=0 border="0" width="100%">
    <tr>
      <th class="fmt_label_width_long">研究課題名</th>
      <td colspan="3">
        <?php echo $shoshikiutil->value($shinsei,'exam_name_nosub', 'svalue') ?>
      </td>
    </tr>
    <tr>
      <th class="fmt_label_width_long">臨床研究登録ID</th>
      <td colspan="3">
        <div><?php echo $shoshikiutil->value($shinsei,'rinsho_db_id_nosub', 'svalue') ?></div>
        <div style="font-size:smaller;">*臨床研究計画公開データベースより付与された登録ＩＤを記載する</div>
      </td>
    </tr>
  </table>
  <br/>

  <table class="table_fmt" cellspacing=0 cellpadding=0 border="0" width="100%">
    <tr>
      <th class="fmt_label_width_long">
        発生機関
      </th>
      <td>
        <div>
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'hassei_kikan_nosub', 'svalue' ))) ?> 自施設
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("2", $shoshikiutil->value($shinsei, 'hassei_kikan_nosub', 'svalue' ))) ?> 他の共同研究機関
          （機関名：<?php echo $shoshikiutil->value($shinsei, 'hassei_kikan_remarks_nosub', 'svalue') ?>）
        </div>
        <div style="font-size:smaller;">＊他の共同研究機関の場合は、以下共同研究機関からの報告書類の添付も可</div>
      </td>
    </tr>
    <tr>
      <th class="fmt_label_width_long">
        識別コード<br />（自施設の場合）
      </th>
      <td>
        <?php echo $shoshikiutil->value($shinsei, 'patient_code_nosub', 'svalue') ?>
      </td>
    </tr>
  </table>
  <br/>
  
  
  <div class="fmt_category_title">重篤な有害事象発現者の情報</div>
  <table class="table_fmt" cellspacing=0 cellpadding=0 border="0" width="100%">
    <tr>
      <td rowspan="2" width="23%">
        重篤な有害事象発現者の区分<br />
        <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'patient_div_is_patient_nosub', 'svalue' ))) ?> 被験者<br />
        <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'patient_div_other_nosub', 'svalue' ))) ?> その他
        （<?php echo $shoshikiutil->value($shinsei, 'patient_div_other_remarks_nosub', 'svalue') ?>）
      </td>

      <td width="17%">
        体重：　
        <?php echo $shoshikiutil->value($shinsei, 'patient_weight_nosub', 'svalue') ?>kg<br />
        身長：　
        <?php echo $shoshikiutil->value($shinsei, 'patient_height_nosub', 'svalue') ?>cm
      </td>
      <td width="30%">
      生年月日 
      <?php echo $dateex->datecb($shoshikiutil->value($shinsei,'patient_birthday_nosub', 'svalue')) ?><br />
      (胎児週齢
      <?php echo $shoshikiutil->value($shinsei,'patient_taiji_syurei_nosub', 'svalue') ?>週
      )
      </td>
      <td width="30%">
        <div>性別：</div>
        <div>
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'patient_sex_nosub', 'svalue' ))) ?> 男
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("2", $shoshikiutil->value($shinsei, 'patient_sex_nosub', 'svalue' ))) ?> 女
        </div>
      </td>
    </tr>
    <tr>
      <td colspan="3">
        被験者の体質：過敏症素因　
        <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'patient_taishitsu_nosub', 'svalue' ))) ?> 無し
        <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("9", $shoshikiutil->value($shinsei, 'patient_taishitsu_nosub', 'svalue' ))) ?> 有り
        （<?php echo $shoshikiutil->value($shinsei, 'patient_taishitsu_contents_nosub', 'svalue') ?>）
      </td>
    </tr>
  </table>
  <br />
  
  <?php $maxCount = 1;
    $count = 1;
    $arr = $shinsei;
    do {
      $name = 'yugai_jisyo_name_nosub_'.$count;
      
      if( array_key_exists($name,$arr))
      {
        $count++;
      }else{
        break;
      }
    } while(1);
    if ($maxCount < ($count-1) )
    {
      $maxCount = $count-1;
    }
  ?>
  <?php //for($i = 1; $i <= $maxCount ;$i++): ?>
  <?php $i = 1 ?>

  <div class="fmt_category_wrapper" id="jutokuContent_<?php echo $i; ?>">
    
    <div class="fmt_left fmt_category_title">重篤な有害事象等に関する情報</div>
    <table class="table_fmt table_list" cellspacing=0 cellpadding=0 border="0" width="100%">
      <tr>
        <th width="25%">
          <div>有害事象名(診断名)</div>
          <div style="font-size:smaller;">医療機器に対する予測の可能性 *</div>
        </th>
        <th width="25%">発現日</th>
        <th width="25%">重篤と判断した理由<br />（複数選択可）</th>
        <th width="25%">有害事象の転帰<br />転帰日</th>
      </tr>
      <tr>
        <td>
          <?php echo $shoshikiutil->value($shinsei, 'yugai_jisyo_name_nosub_'.$i, 'svalue') ?><br />
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_kichi_michi_nosub_'.$i, 'svalue' ))) ?> 既知
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("2", $shoshikiutil->value($shinsei, 'yugai_jisyo_kichi_michi_nosub_'.$i, 'svalue' ))) ?> 未知
        </td>

        <td>
          <?php echo $date->datecb($shoshikiutil->value($shinsei,'yugai_jisyo_hatsugen_date_nosub_'.$i, 'svalue')) ?>
        </td>
        
        <td>
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_die_nosub_'.$i, 'svalue' ))) ?> 死亡
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_die_osore_nosub_'.$i, 'svalue' ))) ?> 死亡の恐れ
          <br />
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_nyuin_nosub_'.$i, 'svalue' ))) ?> 入院、入院期間の延長
          <br />
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_syogai_nosub_'.$i, 'svalue' ))) ?> 障害
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_syogai_osore_nosub_'.$i, 'svalue' ))) ?> 障害の恐れ
          <br />
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_jyutoku_nosub_'.$i, 'svalue' ))) ?> 上記に準じて重篤
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_senten_nosub_'.$i, 'svalue' ))) ?> 先天異常
        </td>
        <td>
        
          <?php echo $date->datecb($shoshikiutil->value($shinsei,'yugai_jisyo_tenki_date_nosub_'.$i, 'svalue')) ?><br />
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_recover_nosub_'.$i, 'svalue' ))) ?> 回復
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_keikai_nosub_'.$i, 'svalue' ))) ?> 軽快
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_mikaihuku_nosub_'.$i, 'svalue' ))) ?> 未回復
          <br />
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_koisyo_nosub_'.$i, 'svalue' ))) ?> 後遺症あり
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_die_nosub_'.$i, 'svalue' ))) ?> 死亡
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_unknown_nosub_'.$i, 'svalue' ))) ?> 不明
          <br />
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'yugai_jisyo_tenki_gaitosezu_nosub_'.$i, 'svalue' ))) ?> 該当せず
        </td>
      </tr>
    </table>
    <div>
      <p style="font-size:smaller;">＊：研究計画書の記載に基づいて判断する。記載内容と性質や重症度が一致する場合は「既知」に該当する。</p>
      <p style="padding-left:2em;font-size:smaller;">記載されていてもその性質や重症度が記載内容と一致しない場合（急性腎不全に対する“間質性腎炎”、肝炎に対する“劇症肝炎”等）は「未知」に該当する。</p>
    </div>
    
    <div class="fmt_category_title">医療機器に関する情報等</div>
    <table class="table_fmt table_list" cellspacing=0 cellpadding=0 border="0" width="100%">
      <tr>
        <th width="25%" colspan="2">
          適応期間
        </th>
        <td>
          <div style="float:left;">
            <?php echo $date->datecb($shoshikiutil->value($shinsei,'drug_toyo_start_date_nosub_'.$i, 'svalue')) ?>　～　
          </div>
          <div style="float:left;">
            <div>
              <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'drug_toyo_is_end_nosub_'.$i, 'svalue' ))) ?> 
              <?php echo $date->datecb($shoshikiutil->value($shinsei,'drug_toyo_end_date_nosub_'.$i, 'svalue')) ?>
            </div>
            <div>
              <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'drug_toyo_is_toyo_chu_nosub_'.$i, 'svalue' ))) ?> 適応中
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <th colspan="2">
          有害事象発現後の措置
        </th>
        <td>
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'drug_sochi_nosub_'.$i, 'svalue' ))) ?> 無
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("9", $shoshikiutil->value($shinsei, 'drug_sochi_nosub_'.$i, 'svalue' ))) ?> 有
         （<?php echo $shoshikiutil->value($shinsei, 'drug_sochi_remarks_nosub_'.$i, 'svalue') ?>）
        </td>
      </tr>
      <tr>
        <th colspan="2">
          医療機器の<br />不具合状態
        </th>
        <td>
          <div style="font-size:smaller;">
            (不具合のあった医療機器等と、その状態（構造的・材質的・機能的欠陥について具体的に記載）
          </div>
          <div>
            <?php echo nl2br($shoshikiutil->value($shinsei, 'drug_fuguai_jyotai_nosub_'.$i, 'svalue')) ?>
          </div>
        </td>
      </tr>
      <tr>
        <th colspan="2">
          不具合が発生した医療機器
        </th>
        <td>
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'drug_hassei_kiki_kenkyutaisyo_nosub_'.$i, 'svalue' ))) ?> 研究対象の機器
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'drug_hassei_kiki_other_nosub_'.$i, 'svalue' ))) ?> その他
         （<?php echo $shoshikiutil->value($shinsei, 'drug_hassei_kiki_remarks_nosub_'.$i, 'svalue') ?>）
        </td>
      </tr>
      <tr>
        <th rowspan="2">
          因果<br />関係
        </th>
        <th>
          有害事象と医療機器
        </th>
        <td>
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'drug_inga_hitei_dekinai_nosub_'.$i, 'svalue' ))) ?> 
          否定できない　
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'drug_inga_hitei_dekiru_nosub_'.$i, 'svalue' ))) ?> 
          否定できる　
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'drug_inga_unknown_nosub_'.$i, 'svalue' ))) ?> 
          不明　
          <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'drug_inga_gaitosezu_nosub_'.$i, 'svalue' ))) ?> 
          該当せず
        </td>
      </tr>
      <tr>
        <th>
          有害事象等と<br />その他の事項
        </th>
        <td>
          <div>
            <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'drug_inga_tewaza_nosub_'.$i, 'svalue' ))) ?> 手技
            （<?php echo $shoshikiutil->value($shinsei, 'drug_inga_tewaza_remarks_nosub_'.$i, 'svalue') ?>）
          </div>
          <div>
            <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'drug_inga_genshikkan_nosub_'.$i, 'svalue' ))) ?> 原疾患
            （<?php echo $shoshikiutil->value($shinsei, 'drug_inga_genshikkan_remarks_nosub_'.$i, 'svalue') ?>）
          </div>
          <div>
            <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'drug_inga_heiyoyaku_nosub_'.$i, 'svalue' ))) ?> 併用薬、併用療法
            （<?php echo $shoshikiutil->value($shinsei, 'drug_inga_heiyoyaku_remarks_nosub_'.$i, 'svalue') ?>）
          </div>
          <div>
            <?php echo $shoshikiutil->checkbox($shoshikiutil->exists("1", $shoshikiutil->value($shinsei, 'drug_inga_other_nosub_'.$i, 'svalue' ))) ?> その他
            （<?php echo $shoshikiutil->value($shinsei, 'drug_inga_other_remarks_nosub_'.$i, 'svalue') ?>）
          </div>
        </td>
      </tr>
    </table>
    <br />

    <div class="fmt_category_title">
      経過：
      <span style="font-size:smaller;font-weight:normal;">
      重篤な有害事象等発現までの詳細な時間経過、重篤な有害事象等に対する処置、転帰及び関連情報を含む症例の概要を記載する。
      </span>
    </div>
    <table class="table_fmt table_list" cellspacing=0 cellpadding=0 border="0" width="100%">
      <tr>
        <th width="15%">
          西暦年/月/日
        </th>
        <th width="85%">
          内       容
        </th>
      </tr>
      <?php for($ki=0;$ki<11;$ki++): ?>
      <tr>
        <td style="text-align:center;">
          <?php echo $date->datecb($shoshikiutil->value($shinsei,sprintf('keika_date_%s_nosub_%s',$ki, $i), 'svalue')) ?>
        </td>
        <td>
          <?php echo nl2br($shoshikiutil->value($shinsei, sprintf('keika_contents_%s_nosub_%s',$ki, $i), 'svalue')) ?>
        </td>
      </tr>
      <?php endfor; ?>
    </table>
    <br />
    
    <div class="fmt_category_title">
      コメント：
      <span style="font-size:smaller;font-weight:normal;">
        医療機器との因果関係の判断根拠、並びに、重篤な有害事象の診断、重篤性、併用薬・療法（医療機器を含む）との相互作用等について記載する。
      </span>
    </div>
    <table class="table_fmt table_list" cellspacing=0 cellpadding=0 border="0" width="100%">
      <tr>
        <td>
          <?php echo nl2br($shoshikiutil->value($shinsei, 'comment_nosub', 'svalue')) ?>
        </td>
      </tr>
    </table>

  </div><!-- id=category_wrapper -->
  <?php //endfor; ?>
  <br />

  <table class="table_fmt" cellspacing=0 cellpadding=0 border="0" width="100%">
    <tr>
      <th class="fmt_label_width">
        添付資料
      </th>
      <td>
        <?php echo nl2br($shoshikiutil->value($shinsei, 'tenpu_shiryo_name_nosub', 'svalue')) ?>
      </td>
    </tr>
  </table>
  <br/>

  <table class="table_fmt" cellspacing=0 cellpadding=0 border="0" width="100%">
    <tr>
      <th class="fmt_label_width">
        備考
      </th>
      <td>
        <?php echo nl2br($shoshikiutil->value($shinsei, 'remarks_nosub', 'svalue')) ?>
      </td>
    </tr>
  </table>
  <br/>

</div><!-- id=fmt_contents -->

<div id="fmt_footer">
</div><!-- id=fmt_footer -->

</div><!-- id=fmt_wrapper -->



