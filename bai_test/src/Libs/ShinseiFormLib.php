<?php

namespace App\Libs;

use Cake\Core\Exception\Exception;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\Core\Configure;
//use App\Libs\BaseShinseiFormItemLib;
//use App\Libs\ShinseiFormAddableLib;
//use App\Libs\ShinseiFormCheckboxLib;
//use App\Libs\ShinseiFormCheckboxwithLib;
//use App\Libs\ShinseiFormDate2dateLib;
//use App\Libs\ShinseiFormDateLib;
//use App\Libs\ShinseiFormHiddenLib;
//use App\Libs\ShinseiFormLabelLib;
//use App\Libs\ShinseiFormMulticheckboxLib;
//use App\Libs\ShinseiFormRadioLib;
//use App\Libs\ShinseiFormRadiowithLib;
//use App\Libs\ShinseiFormSelectLib;
//use App\Libs\ShinseiFormSelectexistingLib;
//use App\Libs\ShinseiFormShoshikiLib;
//use App\Libs\ShinseiFormTextLib;
//use App\Libs\ShinseiFormTextareaLib;

class ShinseiFormLib {

    protected $_item_config = null;
    protected $_render_class = null;
    protected $_item_name = null;
    protected $_shoshiki_name = null;
    protected $_values = null;
    protected $_is_project = null;

    const MODEL_NAME = "ShinseiDetail";

    function __construct($is_project, $shinsei_id, $shoshiki_name, $item_name) {

        if ($is_project) {
            $config_name = 'project_shinsei';
        } else {
            $config_name = 'shinsei';
        }

        $this->_item_config = Configure::read(
                        sprintf($config_name . '.%s.items.%s', $shoshiki_name, $item_name)
        );

        if (!$this->_item_config) {
            throw new ShinseiFormException(sprintf("No item configuration with shoshiki_name=%s, item_name=%s", $shoshiki_name, $item_name));
        }

        if (!isset($this->_item_config['nosub']['type'])) {
            throw new ShinseiFormException(sprintf("No form_type with shoshiki_name=%s, item_name=%s", $shoshiki_name, $item_name));
        }
        $form_type = $this->_item_config['nosub']['type'];
        $classname = sprintf('App\Libs\ShinseiForm%sLib', ucwords($form_type));

        if ($classname == 'App\Libs\ShinseiFormShoshikiLib') {
            $this->_render_class = new $classname(self::MODEL_NAME, $this->_item_config, $is_project);
            $this->_values['shoshiki']['edit_view'] = $this->_item_config['nosub']['options']['edit_view'];
            $this->_values['shoshiki']['detail_view'] = $this->_item_config['nosub']['options']['detail_view'];
        } else {
            $this->_render_class = new $classname(self::MODEL_NAME, $this->_item_config, $is_project);

            $this->_item_name = $item_name;
            $this->_shoshiki_name = $shoshiki_name;
            $this->_is_project = $is_project;
            if($form_type == 'checkboxwith'){
//                pr($this->_render_class);
            }
            // データ初期化
            $this->_values = array();
            foreach ($this->_item_config as $k => $v) {
                $id = uniqid();

                $this->_values[$id]['id'] = null;
                if ($this->_is_project) {
                    $this->_values[$id]['project_shinsei_id'] = $shinsei_id;
                } else {
                    $this->_values[$id]['shinsei_id'] = $shinsei_id;
                }
                $this->_values[$id]['shoshiki_name'] = $shoshiki_name;
                $this->_values[$id]['ssection'] = $item_name;
                $this->_values[$id]['skey'] = $k;
                $this->_values[$id]['occurs'] = null;

                $dv = isset($v["options"]["default"]) ? $v["options"]["default"] : null;

                $this->_values[$id]['svalue'] = $dv;
            }
        }
    }

    function add($key, $value) {
        if ($value['ssection'] !== $this->_item_name) {
            throw new ShinseiFormException("Invalid data was add.");
        }

        // defaultにあれば置き換える
        // なければ追加
        $id = $this->has($value);
        if ($id) {
            $this->_values[$id] = $value;
        } else {
            $this->_values[uniqid()] = $value;
        }
    }

    protected function has($value) {
        $id = null;

        if ($this->_is_project) {
            $key_name = 'project_shinsei_id';
        } else {
            $key_name = 'shinsei_id';
        }

        foreach ($this->_values as $k => $v) {
            if (( $v[$key_name] === $value[$key_name]) && ($v['shoshiki_name'] === $value['shoshiki_name']) && ($v['ssection'] === $value['ssection']) && ($v['skey'] === $value['skey']) && ($v['occurs'] === $value['occurs'])) {
                $id = $k;
                break;
            }
        }

        return $id;
    }

    function isRequired() {
        $r = false;
        if (isset($this->_item_config['nosub']['options']['required'])) {
            $r = $this->_item_config['nosub']['options']['required'];
        }

        return $r;
    }

    /*
      入力項目を出力する
     */

    function renderForm() {
        if (!empty($this->_values)) {
            return $this->_render_class->renderForm($this->_values);
        } else {
            
        }
    }

    /*
      表示タイトルを出力する
     */

    function renderTitle() {
        if (isset($this->_item_config['nosub']['title'])) {
            $r = $this->_item_config['nosub']['title'];
            if (!empty($this->_item_config['nosub']['options']['comment'])) {
                if ($this->_item_config['nosub']['type'] == 'label') {
                    $r = sprintf("%s <span style=\"font-weight:normal;\">（%s）</span>", $r, $this->_item_config['nosub']['options']['comment']);
                } else {
                    if (empty($this->_item_config['nosub']['title']) || $this->_item_config['nosub']['title'] == ' ') {
                        $r = sprintf("（%s）", $this->_item_config['nosub']['options']['comment']);
                    } else {
                        $r = $this->_item_config['nosub']['title'];
                    }
                }
            }
            $r = trim(str_replace("　", " ", $r));

            return $r;
        } else {
            return null;
        }
    }

    function render() {
        if (!empty($this->_values)) {
            return $this->_render_class->render($this->_values);
        }
    }

    function renderComment() {
        if (isset($this->_item_config['nosub']['comment'])) {
            return nl2br($this->_item_config['nosub']['comment']);
        } else {
            return null;
        }
    }

    function renderUpperComment() {
        if (isset($this->_item_config['nosub']['upper_comment'])) {
            return nl2br($this->_item_config['nosub']['upper_comment']);
        } else {
            return null;
        }
    }

    function renderPath() {
        if (isset($this->_values['shoshiki'])) {
            return ($this->_values['shoshiki']);
        } else {
            return null;
        }
    }

}

class ShinseiFormException extends Exception {
    
}
