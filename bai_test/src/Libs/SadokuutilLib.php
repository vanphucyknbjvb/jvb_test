<?php

namespace App\Libs;

use Cake\Core\Configure;

class SadokuutilLib {

    var $COLOR_NUM = 5; //cssで設定している投稿背景の色数

    //ディスカッションの投稿者名を表示するかどうかを判断
    //公開しない場合は「非公開」を返す

    public function getPublicName($sadoku_user, $my_id, $used_type, $shinsei_user_id) {
        if ($my_id == $sadoku_user['id'] || $used_type[51]['member'] == 1 || $used_type[51]['shinsei'] || $used_type[51]['su'] || $shinsei_user_id == $sadoku_user['id']) {
//    if($my_id == $sadoku_user['id'] || $used_type[51]['member'] == 1 || $used_type[51]['shinsei'] || $used_type[51]['su']  )
            $public_name = $sadoku_user['name'] . '（' . $sadoku_user['department_name'] . '）';
        } else
            $public_name = '非公開ユーザー';

        return $public_name;
    }

    //ディスカッションの投稿者ごとの背景色キーを返す
    //名前が非公開の場合は灰色で
    public function getPostGradColor($sadoku_user, $my_id, $used_type, $index_color, $shinsei_user_id) {

        if ($this->getPublicName($sadoku_user, $my_id, $used_type, $shinsei_user_id) == '非公開ユーザー') {
            return 99;
        } elseif (array_key_exists($sadoku_user['id'], $index_color)) {
            $index = $index_color[$sadoku_user['id']];
            $index = $index % 5;

            return $index;
        } else {
            return 5;
        }
    }

    //ディスカッション編集権限の結果取得
    //編集権限trueの条件
    //$post_user_idと$my_idがnullの場合、投稿者による制限はなし
    public function isEditRight($used_type, $i_am, $key, $post_user_id = null, $my_id = null) {
        if ($post_user_id == null && $i_am['jim'] == 1)
            return true;

        $flag = false;

        if ($post_user_id == $my_id) {
            if ($i_am['reviewer'] == 1) {
                if ($used_type[$key]['own'] == 2)
                    $flag = true;
            }
            elseif ($i_am['shinsei'] == 1) {
                if ($used_type[$key]['shinsei'] == 2)
                    $flag = true;
            }
        }

        return $flag;
    }

    /*
     * 削除権限trueの条件
     * （　（自分の投稿で入力権限があること）　または　事務局　）　かつ　当該投稿に返信がないこと
     */

    public function isDeleteRight($post_user_id, $my_id, $used_type, $sadoku_child, $i_am, $key) {
        if (!empty($sadoku_child))
            return false;

        $flag = $this->isEditRight($used_type, $i_am, $key, $post_user_id, $my_id);

        if ($flag || $i_am['jim'] == 1)
            return true;
        else
            false;
    }

    /*
     * 事前審査委員会各メニューへのアクセス可能判定
     */

    public function isAccessMenu($urls, $used_type, $i_am) {
        if ($i_am['jim'] == 1)
            return true;

        $acl = Configure::read('sadoku.common.sadokuhan_rights.access.menu');
        $flag_page = false;
        $flag_mem = false;

        //全ての権限が0ならアクセス不可
        foreach ($acl[$urls] as $key) {
            if ($used_type[$key])
                $flag_page = true;

            if ($i_am['reviewer'] == 1) {
                if ($used_type[$key]['own'] != 0)
                    $flag_mem = true;
            }
            elseif ($i_am['shinsei'] == 1) {
                if ($used_type[$key]['shinsei'] != 0)
                    $flag_mem = true;
            }
        }

        return $flag_page && $flag_mem;
    }

    /*
     * 事前審査委員会権限　参照可能判定
     */

    public function isViewed($used_type, $i_am, $key) {
        if ($i_am['jim'] == 1)
            return true;

        $flag_page = false;
        $flag_mem = false;

        //権限が1以上なら参照可能
        if ($used_type[$key])
            $flag_page = true;

        if ($i_am['reviewer'] == 1) {
            if ($used_type[$key]['own'] >= 1)
                $flag_mem = true;
        }
        elseif ($i_am['shinsei'] == 1) {
            if ($used_type[$key]['shinsei'] >= 1)
                $flag_mem = true;
        }

        return $flag_page && $flag_mem;
    }

}
