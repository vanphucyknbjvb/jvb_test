<?php

namespace App\Libs;

use App\Libs\BaseShinseiFormItemLib;

class ShinseiFormSelectLib extends BaseShinseiFormItemLib {

    function renderForm($values = array()) {
        $result = parent::renderForm($values);

        $keys = array_keys($values);
        $key = $keys[0];
        $skey = $values[$key]['skey'];

        $form_options = $this->_item_config[$skey]['form_options'] + array('multiple' => false, 'empty' => false)
        ;

        $options = $this->options_key_value($this->_item_config[$skey]['options']['options']);

        $result .= $this->_Form->select(
                sprintf("%s.%s.svalue", $this->_model_name, $key)
                , $options
                , explode("|", $values[$key]['svalue'])
                , $form_options
        );

        return $result;
    }

}
