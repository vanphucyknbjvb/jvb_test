$(document).ready(function() {
    $(".back").click(function(){
        if(confirm("Chưa sửa bạn có muốn quay lại?")){ 
            history.back(1);
        }
    });

});

//image0
var srcImg0 = $('#preview_avatar0').attr('src');
$(".image-preview-input0 input:file").change(function (){
    var countFiles = $(this)[0].files.length;
    var filesize = this.files[0].size;
    var imgPath = $(this)[0].value;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if (countFiles > 1) {
        alert('Chỉ chọn một ảnh.');
    } else if(countFiles === 0){
        alert("Chưa chọn ảnh.");
    } else {
        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if(filesize > 1048576){
                alert("Yêu cầu chọn ảnh có dung lượng không quá 1024Kb.");
            }else{              
                var img = $('#preview_avatar0');
                var file = this.files[0];
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(".image-preview-clear0").show();
                    $('#preview_avatar0').show();
                    img.attr('src', e.target.result);
                }
                reader.readAsDataURL(file);
                $("#image-holder0").css("display","block");
            }               

        } else {
            alert("File không được hỗ trợ.");
        }
    }
    
});
// Clear event
$('.image-preview-clear0').click(function(){
    $(".image-preview-clear0").hide();
    $('.image-preview-input0 input:file').val('');
    var img = $('#preview_avatar0');
    if(typeof srcImg0 === "undefined"){
        img.attr('src', '');
        $('#preview_avatar0').hide();
    }else{
        img.attr('src', srcImg0);
    }
}); 

//image1
var srcImg1 = $('#preview_avatar1').attr('src');
$(".image-preview-input1 input:file").change(function (){
    var countFiles = $(this)[0].files.length;
    var filesize = this.files[0].size;
    var imgPath = $(this)[0].value;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if (countFiles > 1) {
        alert('Chỉ chọn một ảnh.');
    } else if(countFiles === 0){
        alert("Chưa chọn ảnh.");
    } else {
        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if(filesize > 1048576){
                alert("Yêu cầu chọn ảnh có dung lượng không quá 1024Kb.");
            }else{              
                var img = $('#preview_avatar1');
                var file = this.files[0];
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(".image-preview-clear1").show();
                    $('#preview_avatar1').show();
                    img.attr('src', e.target.result);
                }
                reader.readAsDataURL(file);
                $('#preview_avatar1').show();
                $("#image-holder1").css("display","block");
            }               

        } else {
            alert("File không được hỗ trợ.");
        }
    }
    
});
// Clear event
$('.image-preview-clear1').click(function(){
    $(".image-preview-clear1").hide();
    $('.image-preview-input1 input:file').val('');
    var img = $('#preview_avatar1');
    if(typeof srcImg1 === "undefined"){
        img.attr('src', '');
        $('#preview_avatar1').hide();
    }else{
        img.attr('src', srcImg1);
    }
}); 

//image2
var srcImg2 = $('#preview_avatar2').attr('src');
$(".image-preview-input2 input:file").change(function (){
    var countFiles = $(this)[0].files.length;
    var filesize = this.files[0].size;
    var imgPath = $(this)[0].value;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if (countFiles > 1) {
        alert('Chỉ chọn một ảnh.');
    } else if(countFiles === 0){
        alert("Chưa chọn ảnh.");
    } else {
        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if(filesize > 1048576){
                alert("Yêu cầu chọn ảnh có dung lượng không quá 1024Kb.");
            }else{              
                var img = $('#preview_avatar2');
                var file = this.files[0];
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(".image-preview-clear2").show();
                    $('#preview_avatar2').show();
                    img.attr('src', e.target.result);
                }
                reader.readAsDataURL(file);
                $("#image-holder2").css("display","block");
            }               

        } else {
            alert("File không được hỗ trợ.");
        }
    }
    
});
// Clear event
$('.image-preview-clear2').click(function(){
    $(".image-preview-clear2").hide();
    $('.image-preview-input2 input:file').val('');
    var img = $('#preview_avatar2');
    if(typeof srcImg2 === "undefined"){
        img.attr('src', '');
        $('#preview_avatar2').hide();
    }else{
        img.attr('src', srcImg2);
    }
});

//image3
var srcImg3 = $('#preview_avatar3').attr('src');
$(".image-preview-input3 input:file").change(function (){
    var countFiles = $(this)[0].files.length;
    var filesize = this.files[0].size;
    var imgPath = $(this)[0].value;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if (countFiles > 1) {
        alert('Chỉ chọn một ảnh.');
    } else if(countFiles === 0){
        alert("Chưa chọn ảnh.");
    } else {
        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if(filesize > 1048576){
                alert("Yêu cầu chọn ảnh có dung lượng không quá 1024Kb.");
            }else{              
                var img = $('#preview_avatar3');
                var file = this.files[0];
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(".image-preview-clear3").show();
                    $('#preview_avatar3').show();
                    img.attr('src', e.target.result);
                }
                reader.readAsDataURL(file);
                $("#image-holder3").css("display","block");
            }               

        } else {
            alert("File không được hỗ trợ.");
        }
    }
    
});
// Clear event
$('.image-preview-clear3').click(function(){
    $(".image-preview-clear3").hide();
    $('.image-preview-input3 input:file').val('');
    var img = $('#preview_avatar3');
    if(typeof srcImg3 === "undefined"){
        img.attr('src', '');
        $('#preview_avatar3').hide();
    }else{
        img.attr('src', srcImg3);
    }    
});

//image4
var srcImg4 = $('#preview_avatar4').attr('src');
$(".image-preview-input4 input:file").change(function (){
    var countFiles = $(this)[0].files.length;
    var filesize = this.files[0].size;
    var imgPath = $(this)[0].value;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if (countFiles > 1) {
        alert('Chỉ chọn một ảnh.');
    } else if(countFiles === 0){
        alert("Chưa chọn ảnh.");
    } else {
        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if(filesize > 1048576){
                alert("Yêu cầu chọn ảnh có dung lượng không quá 1024Kb.");
            }else{              
                var img = $('#preview_avatar4');
                var file = this.files[0];
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(".image-preview-clear4").show();
                    $('#preview_avatar4').show();
                    img.attr('src', e.target.result);
                }
                reader.readAsDataURL(file);
                $("#image-holder4").css("display","block");
            }               

        } else {
            alert("File không được hỗ trợ.");
        }
    }
    
});
// Clear event
$('.image-preview-clear4').click(function(){
    $(".image-preview-clear4").hide();
    $('.image-preview-input4 input:file').val('');
    var img = $('#preview_avatar4');
    if(typeof srcImg4 === "undefined"){
        img.attr('src', '');
        $('#preview_avatar4').hide();
    }else{
        img.attr('src', srcImg4);
    }    
});
