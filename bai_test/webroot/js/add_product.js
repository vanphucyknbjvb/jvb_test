//image
$(document).ready(function() {
    document.getElementById('pro-image0').addEventListener('change', readImage, false);	    
    $( ".preview-images-zone" ).sortable();	    
    $(document).on('click', '.image-cancel', function() {
        let no = $(this).data('no');
        $(".preview-image.preview-show-"+no).remove();
        maxImg -= 1;
        if(maxImg == 1){
            $(".preview-images-product").css("display","none");
        }       
    });

    $(".back").click(function(){
        if(confirm("Chưa tạo sản phẩm bạn có muốn quay về trang danh sách phẩm")){ 
            window.location.href = "/admin/products/list_product";
        }
    });

    $( "#title" ).change(function() {
        $.ajax({
            url : '/admin/products/to_slug',
            type : 'post',
            dataType:'json',
            data : {
                data : $('#title').val()
            },
            success : function (result){
                $('#slug').val(result);
            }
        });
    });    

});

function slugcheck(val) { 
    var slug = $(val).val(); 
    var slug = slug.replace(/[/?\#\%\+]/gi, '') 
    val.value = slug; 
} 

var num = 1;
var maxImg = 1;
function readImage() {
    if (window.File && window.FileList && window.FileReader) {
        var files = event.target.files || event.dataTransfer.files || event.dataTransfer.getData;

        var output = $(".preview-images-zone");
        for (let i = 0; i < files.length; i++) {
            var imgPath = files[i]['name'];
            var alertNumber = true;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            if(extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg"){
                //validate width vs height
                if(files[i]['size'] <= 524288){
                    var _URL = window.URL || window.webkitURL;
                    sezi_filee = this.files[i];
                    img = new Image();
                    img.onload = function() {
                        var width = this.width;
                        var height =this.height;
                        if(width != height){
                            alert('Kích thước ảnh ' + files[i]['name'] + ' phải là 1:1');
                        }else {                   
                            var picReader = new FileReader();
                            picReader.onloadend = function () {
                                if(maxImg <= 5){
                                    var html =  '<div class="preview-image preview-show-' + num + '">' +
                                                '<div class="image-cancel" data-no="' + num + '">x</div>' +
                                                '<input name="' + num + '[]" style="display:none;" type="text" class="form_mini" value="' + num + '">' +
                                                '<input name="' + num + '[]" style="display:none;" type="text" value="' + files[i]['name'] + '">' +
                                                '<div class="image-zone"><img id="pro-img-' + num + '" src="' + picReader.result + '"></div>';
                                    output.append(html);
                                    $(".preview-images-product").css("display","block");
                                    num += 1;
                                    maxImg += 1;
                                }else{
                                    if(alertNumber != false){
                                        alert('Số lượng ảnh không được vượt quá 5');
                                        alertNumber = false;
                                    }
                                }    
        		            };
        		            picReader.readAsDataURL(files[i]);
        		        }
                    };
                    img.src = _URL.createObjectURL(sezi_filee);
                }else {
                    alert('Ảnh ' + files[i]['name'] + ' vượt quá 512Kb');
                }
            }else{
                alert('Ảnh ' + files[i]['name'] + ' không phải là file ảnh');
            }           
        }
        var nimage = $("#input-image").val();
        $("#click-image").attr("onclick","$('#pro-image"+nimage+"').click()");
        $(".file").append('<input name="imagess[]" style="display:none;" id="pro-image' + nimage + '" type="file" multiple>');
        var abc = 'pro-image'+nimage;
        document.getElementById(abc).addEventListener('change', readImage, false);
        $('#input-image').val(++nimage);
        // $("#pro-image").val('');
    } else {
        console.log('Browser not support');
    }
}

//select multiple
$(document).ready(function() {
    let branch_all = [];
    
    function formatResult(state) {
        branch_all.push(state.id);
        var id = 'state' + state.id;
        var checkbox = $('<div class="checkbox"><input id="'+id+'" type="checkbox" '+(state.selected ? 'checked': '')+'><label for="checkbox1">'+state.text+'</label></div>', { id: id });
        return checkbox;   
    }
    
    function arr_diff(a1, a2) {
        var a = [], diff = [];
        for (var i = 0; i < a1.length; i++) {
            a[a1[i]] = true;
        }
        for (var i = 0; i < a2.length; i++) {
            if (a[a2[i]]) {
                delete a[a2[i]];
            } else {
                a[a2[i]] = true;
            }
        }
        for (var k in a) {
            diff.push(k);
        }
        return diff;
    }
    
    let optionSelect2 = {
        templateResult: formatResult,
        closeOnSelect: false,
        width: '100%'
    };
    
    let $select2 = $("#country").select2(optionSelect2);
    
    var scrollTop;
    
    $select2.on("select2:selecting", function( event ){
        var $pr = $( '#'+event.params.args.data._resultId ).parent();
        scrollTop = $pr.prop('scrollTop');
    });
    
    $select2.on("select2:select", function( event ){
        $(window).scroll();
        
        var $pr = $( '#'+event.params.data._resultId ).parent();
        $pr.prop('scrollTop', scrollTop );
        
        $(this).val().map(function(index) {
            $("#state"+index).prop('checked', true);
        });
    });
    
    $select2.on("select2:unselecting", function ( event ) {
        var $pr = $( '#'+event.params.args.data._resultId ).parent();
        scrollTop = $pr.prop('scrollTop');
    });
    
    $select2.on("select2:unselect", function ( event ) {
        $(window).scroll();
        
        var $pr = $( '#'+event.params.data._resultId ).parent();
        $pr.prop('scrollTop', scrollTop );
        
        var branch  =   $(this).val() ? $(this).val() : [];
        var branch_diff = arr_diff(branch_all, branch);
        branch_diff.map(function(index) {
            $("#state"+index).prop('checked', false);
        });
    });
});

//format number money
(function($, undefined) {
    "use strict";
    $(function() {            
        var $form = $( ".addProduct" );
        var $input = $form.find( ".inputNumber" );
        $input.on( "keyup", function( event ) {
            var selection = window.getSelection().toString();
            if ( selection !== '' ) {
                return;
            }                
            // When the arrow keys are pressed, abort.
            if ( $.inArray( event.keyCode, [38,40,37,39] ) !== -1 ) {
                return;
            }
                            
            var $this = $( this );
            
            // Get the value.
            var input = $this.val();
            
            var input = input.replace(/[\D\s\._\-]+/g, "");
                    input = input ? parseInt( input, 10 ) : 0;

                    $this.val( function() {
                        return ( input === 0 ) ? "" : input.toLocaleString( "en-US" );
                    } );
        } );
        
        /**
         * ==================================
         * When Form Submitted
         * ==================================
         */
        $form.on( "submit", function( event ) {
            
            var $this = $( this );
            var arr = $this.serializeArray();
        
            for (var i = 0; i < arr.length; i++) {
                    arr[i].value = arr[i].value.replace(/[($)\s\._\-]+/g, '1'); // Sanitize the values.
            };
            
            console.log( arr );
            
            // event.preventDefault();
        });
        
    });
})(jQuery);
