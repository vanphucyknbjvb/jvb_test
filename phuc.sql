-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2020 at 11:01 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bai_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `describes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `describes`, `content`, `url_image`, `deleted`, `status`, `created`, `modified`) VALUES
(1, 'test 12', 'hehe', '<p>Theo số liệu thống k&ecirc; sơ bộ của Tổng cục Hải quan, trong th&aacute;ng 4/2020 s&ocirc;́ lượng &ocirc; t&ocirc; nguy&ecirc;n chiếc c&aacute;c loại đăng k&iacute; l&agrave;m thủ tục hải quan nhập khẩu v&agrave;o Việt Nam đạt 4.918 chi&ecirc;́c, tương ứng đạt 131 tri&ecirc;̣u&nbsp;<a href=\"https://www.24h.com.vn/ty-gia-ngoai-te-ttcb-c426.html\" title=\"USD\">USD</a>. Như vậy, lượng xe nhập về Việt Nam đ&atilde; giảm tới gần 60% (tương ứng giảm 7.233 chiếc) so với th&aacute;ng 3. Gi&aacute; trung b&igrave;nh mỗi xe nhập v&agrave;o Việt nam l&agrave; gần 622 triệu đồng/chiếc.</p>\r\n\r\n<p>Đ&acirc;y l&agrave; 1 trong 2&nbsp;th&aacute;ng c&oacute; lượng &ocirc; t&ocirc; nguy&ecirc;n chiếc nhập khẩu &iacute;t nhất kể từ đầu năm 2020.</p>\r\n\r\n<p><img alt=\"Ô tô về Việt Nam giảm mạnh, giá tăng hàng trăm triệu đồng/xe - 1\" src=\"https://cdn.24h.com.vn/upload/2-2020/images/2020-05-15/o-to-ve-Viet-Nam-it-dan-gia-tang-hang-tram-trieu-chiec-xe-chuan-1589525543-764-width578height289.png\" /></p>\r\n\r\n<p>Th&aacute;ng 4&nbsp;c&oacute; lượng nhập khẩu &ocirc; t&ocirc; rất thấp&nbsp;</p>\r\n\r\n<p>Lũy kế lượng nhập khẩu &ocirc;t&ocirc; trong 4 th&aacute;ng đầu năm 2020 ước đạt 33.000 chiếc v&agrave; trị gi&aacute; l&agrave; 710 triệu USD. So với c&ugrave;ng kỳ năm 2019, lượng nhập khẩu nh&oacute;m h&agrave;ng n&agrave;y trong 4 th&aacute;ng năm 2020 giảm 35,2% về lượng v&agrave; giảm 36,6% về trị gi&aacute;. T&iacute;nh trung b&igrave;nh mỗi chiếc c&oacute; trị gi&aacute; hơn 502 triệu đồng.</p>\r\n', '/upload/imagesBlogs/21a75d2c-31c0-4e06-932f-1d3d40619108_1589529988_73119.png', 0, 1, '2020-05-15 16:25:30', '2020-05-15 17:57:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `created`, `modified`) VALUES
(1, 'dinhvanphuc5@gmail.com', '$2y$10$.6tGAkxyLNQCeNGgvbF0K..NTmhNRPFQNUXER1/Qx1axz7jwQG2Vi', 'dinhvanphuc5@gmail.com', '2020-05-15 12:37:13', '2020-05-15 12:37:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
